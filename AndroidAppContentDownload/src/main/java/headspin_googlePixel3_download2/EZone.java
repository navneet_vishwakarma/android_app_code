package headspin_googlePixel3_download2;

import java.io.File;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;
import utility.ImagesPDFrelated;
import utility.ReadWrite;

/**
 * EPresse --- program to download EPresse app publications
 * @author NA20251768
 *
 */
public class EZone extends AppRelated {

	/*	

	EZone ----  DONE

	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {572, 1599, 572, 471};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(EZone.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			try {
				MobileElement el1 = driver.findElementById("com.hket.android.ezone:id/close_button");
				el1.click();
			} catch (Exception e) {
				MobileElement el9 = driver.findElementByXPath("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");
				el9.click();
			}
			MobileElement el2 = driver.findElementById("com.hket.android.ezone:id/header_menu_image");
			el2.click();
			MobileElement el3 = driver.findElementById("com.hket.android.ezone:id/drawer_login_login");
			el3.click();
			MobileElement el4 = driver.findElementById("com.hket.android.ezone:id/login_enter_email");
			el4.sendKeys(username());
			MobileElement el5 = driver.findElementById("com.hket.android.ezone:id/login_enter_password");
			el5.sendKeys(password());
			MobileElement el6 = driver.findElementById("com.hket.android.ezone:id/btn_ok");
			el6.click();

			String date = driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/mian_list']/android.widget.LinearLayout[1]//android.widget.TextView[@resource-id='com.hket.android.ezone:id/date']").getText();
			log.info(date);

			MobileElement el7 = driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/mian_list']/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]//android.widget.TextView[@resource-id='com.hket.android.ezone:id/down_allbooks']");
			el7.click();
			
			for (int i = 0; i < 5; i++) {
				try {
					driver.findElementById("sjkhdksd");
				} catch (Exception e) {
					log.info("Taking time");
				}
			}
			
			for(int i = 0; i < 10; i++) {
				int check = driver.findElementsByXPath("//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/mian_list']/android.widget.LinearLayout[1]//android.widget.HorizontalScrollView[1]/android.widget.LinearLayout/android.widget.LinearLayout[1]//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/read_ll']").size();
				if(check != 0) {
					log.info("Loading");
				} else {
					log.info("Image Opened");
					break;
				}
			}
			
			int loop = driver.findElementsByXPath("//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/mian_list']/android.widget.LinearLayout[1]//android.widget.HorizontalScrollView[1]/android.widget.LinearLayout/android.widget.LinearLayout").size();
			log.info("Loop count = " + loop);
			
			img = new ImagesPDFrelated();

			img.deleteFiles(app, keyWord);

			Thread.sleep(5000);
			
			int count = 1;
			for(int j = 1; j <= loop; j++) {
				Thread.sleep(5000);
				MobileElement el8 = driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/mian_list']/android.widget.LinearLayout[1]//android.widget.HorizontalScrollView[1]/android.widget.LinearLayout/android.widget.LinearLayout["+ j +"]//android.widget.LinearLayout[@resource-id='com.hket.android.ezone:id/read_ll']");
				el8.click();
				Thread.sleep(5000);
				
				try {
					MobileElement el9 = driver.findElementByXPath("//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']");
					el9.click();
				} catch (Exception e) {
					log.info("No permission asked");
				}
				
				try {
					MobileElement el10 = driver.findElementByXPath("//android.widget.ImageView");
					el10.click();
				} catch (Exception e){
					MobileElement el10 = driver.findElementByXPath("//android.widget.ImageView");
					el10.click();
				}
				
				String text = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.hket.android.ezone:id/foot_page']").getText();
				
				int imagesCount = Integer.parseInt(text.substring(2, text.length() - 1));

				log.info("The images count is " + imagesCount);
				
				MobileElement el11 = driver.findElementByXPath("//android.widget.Gallery[@resource-id='com.hket.android.ezone:id/mygallery']/android.widget.ImageView");
				el11.click();
				
				rw = new ReadWrite();
				TakesScreenshot sc = driver;
				File src = sc.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P" + count++ +".png"));
				log.info(count + " screenshot taken");

				for(int i = 2; i < imagesCount + 1; i++) {

					(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
					.release().perform();
					
					Thread.sleep(2000);

					sc = driver;
					src = sc.getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P" + count++ +".png"));
					log.info(i + " screenshot taken");

				}
				Thread.sleep(3000);			
				driver.navigate().back();
			}

			img.generatePDF(date, keyWord, app);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
