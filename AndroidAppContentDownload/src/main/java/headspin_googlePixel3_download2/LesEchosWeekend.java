package headspin_googlePixel3_download2;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * LesEchos --- program to download LesEchos app publications
 * @author NA20251768
 *
 */
public class LesEchosWeekend extends AppRelated {

	/*	

	Les Echos ----- DONE
	Les Echos Weekend ----- DONE
	
	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1128, 251, 185, 251};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(LesEchosWeekend.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			Thread.sleep(7000);
			
			MobileElement el1 = driver.findElementById("fr.lesechos.live:id/button_agree");
			el1.click();
			MobileElement el2 = driver.findElementByAccessibilityId("Mon compte");
			el2.click();
			MobileElement el3 = driver.findElementById("fr.lesechos.live:id/profileConnection");
			el3.click();
			MobileElement el4 = driver.findElementById("fr.lesechos.live:id/loginEmail");
			el4.sendKeys(username());
			MobileElement el5 = driver.findElementById("fr.lesechos.live:id/loginPassword");
			el5.sendKeys(password());
			MobileElement el6 = driver.findElementById("fr.lesechos.live:id/emailSignInButton");
			el6.click();
			Thread.sleep(15000);
			driver.navigate().back();
			//MobileElement el7 = driver.findElementByAccessibilityId("Navigate up");
			//el7.click();
			MobileElement el8 = driver.findElementByAccessibilityId("Le Journal");
			el8.click();

			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='Les Echos Weekend']/../following-sibling::android.widget.LinearLayout/android.widget.FrameLayout[1]//android.widget.Button[@text='TÉLÉCHARGER']").size();
				if(check == 0) {
					log.info("Scroll Down again...");
					(new AndroidTouchAction(driver)).press(PointOption.point(515, 1507))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(554, 713))
					.release().perform();
				} else {
					log.info("Found");
					
					break;
				}
			}
			
			MobileElement els5 = driver.findElementByXPath("//android.widget.TextView[@text='Les Echos Weekend']/../following-sibling::android.widget.LinearLayout/android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='fr.lesechos.live:id/journalItemTitle']");
			String date = els5.getText();
			log.info(date);
			
			MobileElement els51 = driver.findElementByXPath("//android.widget.TextView[@text='Les Echos Weekend']/../following-sibling::android.widget.LinearLayout/android.widget.FrameLayout[1]//android.widget.Button[@text='TÉLÉCHARGER']");
			els51.click();
			
			for(int i = 0; i < 20; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='Les Echos Weekend']/../following-sibling::android.widget.LinearLayout/android.widget.FrameLayout[1]//android.widget.Button[@text='LIRE']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Les Echos Weekend']/../following-sibling::android.widget.LinearLayout/android.widget.FrameLayout[1]//android.widget.Button[@text='LIRE']");
			els1.click();
			
			Thread.sleep(3000);
			
			MobileElement els8 = driver.findElementByXPath("//android.widget.ImageView/parent::android.widget.FrameLayout[@index=1]");
			els8.click();
			
			Thread.sleep(3000);
			
			String lastThumbnailXpath = "//android.widget.FrameLayout/preceding-sibling::androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[last()]//android.widget.TextView";
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
					imagesCount = Integer.parseInt(text);

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}
			
			log.info("The images count is " + imagesCount);
			
			MobileElement els13 = driver.findElementByXPath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]//android.widget.ImageView/parent::android.widget.FrameLayout[@index=1]");
			els13.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
