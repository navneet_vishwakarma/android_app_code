package samsung_download1;

import java.time.Duration;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Readly --- program to download Readly app publications
 * @author NA20251768
 *
 */
public class Readly extends AppRelated {

	/*

Allers ----- DONE
Hemmets Journal ---- DONE
Allas ---- DONE
�ret Runt ---- DONE
Land ---- DONE
Icakuriren ---- DONE 
Expressen/KvP/GT S�ndag ---- 

	 */

	static int[] swipeCoordinates = {1495, 1257, 96, 1257};
	static int[] thumbnailSwipeCoord = {172, 2128, 1479, 2128};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Readly.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			MobileElement el1 = driver.findElementById("com.readly.client:id/rdsInternalPrimaryButtonButton");
			el1.click();
			MobileElement el2 = driver.findElementById("com.readly.client:id/rdsInternalPrimaryButtonButton");
			el2.click();
			MobileElement el3 = driver.findElementById("com.readly.client:id/rdsInternalInputEditText");
			el3.sendKeys(username());
			MobileElement el4 = driver.findElementById("com.readly.client:id/rdsInternalPrimaryButtonButton");
			el4.click();
			MobileElement els2 = driver.findElementByXPath("//android.widget.LinearLayout[@resource-id='com.readly.client:id/passwordInput']//android.widget.EditText[@resource-id='com.readly.client:id/rdsInternalInputEditText']");
			els2.sendKeys(password());
			MobileElement el6 = (MobileElement) driver.findElementById("com.readly.client:id/rdsInternalPrimaryButtonButton");
			el6.click();
			MobileElement el7 = (MobileElement) driver.findElementById("com.readly.client:id/profile_name");
			el7.click();
			MobileElement el8 = (MobileElement) driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Search\"]/android.widget.ImageView");
			el8.click();
			MobileElement el10 = (MobileElement) driver.findElementById("com.readly.client:id/rdsInternalInputEditText");
			el10.sendKeys(keyWord);
			el10.click();
			el10.sendKeys(keyWord);
			Thread.sleep(5000);

			KeyInput keyboard = new KeyInput("keyboard");
			Sequence sendKeys = new Sequence(keyboard, 0);
			sendKeys.addAction(keyboard.createKeyDown(Keys.ENTER.getCodePoint()));

			driver.perform(Arrays.asList(sendKeys));

			Thread.sleep(5000);
			
			MobileElement el11 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView[@resource-id='com.readly.client:id/section_recycler_view']//android.widget.TextView[@text='" + keyWord + "']");
			el11.click();
			
			MobileElement els44 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView[@resource-id='com.readly.client:id/context_recycler_view']//android.widget.LinearLayout[@resource-id='com.readly.client:id/cg_issue_item_layout_container'][1]//android.widget.TextView");
			String date = els44.getText();
			log.info(date);
			
			MobileElement el12 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView[@resource-id='com.readly.client:id/context_recycler_view']//android.widget.LinearLayout[@resource-id='com.readly.client:id/cg_issue_item_layout_container'][1]");
			el12.click();
			
			MobileElement el13 = driver.findElementById("com.readly.client:id/reader_page_frame");
			el13.click();
			
			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.readly.client:id/reader_horizontalscroll']//android.view.ViewGroup[last()-1]//android.widget.LinearLayout[@resource-id='com.readly.client:id/preview_thumbs']//android.view.ViewGroup[last()]//android.widget.TextView[@resource-id='com.readly.client:id/thumbnail_pagenumber']";
			String secondThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.readly.client:id/reader_horizontalscroll']//android.view.ViewGroup[last()-2]//android.widget.LinearLayout[@resource-id='com.readly.client:id/preview_thumbs']//android.view.ViewGroup[last()]//android.widget.TextView[@resource-id='com.readly.client:id/thumbnail_pagenumber']";
			String count = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
			String textCount = null;
			for(int j = 0; j < 5; j++) {
				for(int i = 0; i < 10; i++) {

					(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[3]))
					.release().perform();

				}

				Thread.sleep(10000);

				textCount = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

				if(textCount.equals(count))
					break;
				else count = textCount;
			}
			
			int imagesCount = 0;
			try {
				imagesCount = Integer.parseInt(textCount);
			} catch (NumberFormatException n) {
				textCount = driver.findElementByXPath(secondThumbnailXpath).getAttribute("text");
				imagesCount = Integer.parseInt(textCount) + 2;
			}

			log.info("The images count is " + (imagesCount + 1));
			
			
			
			
			
			
			MobileElement checkFirst = null;
			
			for(int j = 0; j < 20; j++) {
				for(int i = 0; i < 6; i++) {

					(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
					.release().perform();

				}

				Thread.sleep(5000);
			
				try {
					checkFirst = driver.findElementByXPath("//android.widget.TextView[@text='1' and @resource-id='com.readly.client:id/thumbnail_pagenumber']");
					if(checkFirst.getText().equals("1")) {
						System.out.println("First Image found");
						break;
					}
				} catch (NoSuchElementException n) {
					System.out.println("Scroll Left");
				}
			}
			
			checkFirst.click();

			tearDown(imagesCount + 1, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}

	}

}
