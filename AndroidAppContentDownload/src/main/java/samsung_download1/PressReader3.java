package samsung_download1;

import org.apache.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * PressReader --- program to download PressReader app publications
 * @author NA20251768
 *
 */
public class PressReader3 extends AppRelated {

	/*

Heat (UK) ----- FV ----- DONE

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(PressReader3.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement els4 = driver.findElementByXPath("//android.widget.TextView[@text='Sign in']");
			els4.click();

			MobileElement el1 = driver.findElementByAccessibilityId("email");
			el1.click();

			MobileElement el2 = driver.findElementByAccessibilityId("authorization_user_name");
			el2.sendKeys(username());

			MobileElement el3 = driver.findElementByAccessibilityId("authorization_user_password");
			el3.sendKeys(password());

			MobileElement el4 = driver.findElementByAccessibilityId("authorization_signin");
			el4.click();

			Thread.sleep(5000);


			for(int i = 0; i < 10; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@resource-id='com.newspaperdirect.pressreader.android:id/loading_status_view_text']").size();

				if(check==0) {
					MobileElement el90 = driver.findElementById("com.newspaperdirect.pressreader.android:id/search_view");
					el90.click();
					log.info("Search Opened");
					break;
				} else {
					Thread.sleep(5000);
					log.info("Loaded again...");
				}
			}

			MobileElement el5 = driver.findElementById("com.newspaperdirect.pressreader.android:id/search_view");
			el5.sendKeys(keyWord);

			MobileElement el18 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView\n[@resource-id='com.newspaperdirect.pressreader.android:id/publications_detailed_items_view']/android.widget.FrameLayout[1]//android.widget.ImageView[@resource-id='com.newspaperdirect.pressreader.android:id/thumbnail']");
			el18.click();

			MobileElement el6 = driver.findElementByAccessibilityId("order_open");
			el6.click();
			String date = null;
			try {
				date = driver.findElementById("com.newspaperdirect.pressreader.android:id/title_current_position").getText();
				log.info(date);
			} catch(StaleElementReferenceException st) {
				log.info("stale found");
			}
			
			for(int i = 0; i < 4; i++) {
				try {
					date = driver.findElementById("com.newspaperdirect.pressreader.android:id/title_current_position").getText();
					log.info(date);
					break;
				} catch(Exception e) {
					log.info("exception1 " + i);
				}
				try {
					MobileElement el8 = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='com.newspaperdirect.pressreader.android:id/single_page_view']");
					el8.click();
					log.info("First image clicked");
				} catch(Exception e) {
					log.info("exception2 " + i);
				}
			}
			
			int checkTip = 0;
			
			for(int i = 0; i < 4; i++) {
				checkTip = driver.findElementsById("com.newspaperdirect.pressreader.android:id/txtTip").size();
				if(checkTip != 0) {
					Thread.sleep(4000);
					driver.findElementById("com.newspaperdirect.pressreader.android:id/txtTip").click();
					log.info("Tool tip clicked");
					//Thread.sleep(10000);
					break;
				}
			}
			
			int checkDate = driver.findElementsById("com.newspaperdirect.pressreader.android:id/title_current_position").size();
			if(checkDate != 0) {
				MobileElement el8 = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='com.newspaperdirect.pressreader.android:id/single_page_view']");
				el8.click();
				log.info("First image clicked");
			}
			
			for(int i = 0; i < 100; i++) {
				int checkProgress = driver.findElementsById("com.newspaperdirect.pressreader.android:id/progressBarDownload").size();
				log.info("Progress count = " + i);
				if(checkProgress == 0) 
					break;
				Thread.sleep(5000);
			}

			int imagesCount = Integer.parseInt(date.substring(date.lastIndexOf(" ") + 1));

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}

	}

}
