package samsung_download1;

import org.apache.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * KioscoLaNacion --- program to download KioscoLaNacion app publications
 * @author NA20251768
 *
 */
public class KioscoLaNacion extends AppRelated {

	/*

HOLA ----- FF ----- DONE

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(KioscoLaNacion.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("com.newspaperdirect.lanacion.android:id/image");
			el1.click();

			MobileElement el2 = driver.findElementById("com.newspaperdirect.lanacion.android:id/authorization_user_name");
			el2.sendKeys(username());

			MobileElement el3 = driver.findElementById("com.newspaperdirect.lanacion.android:id/authorization_password");
			el3.sendKeys(password());

			MobileElement el4 = driver.findElementById("com.newspaperdirect.lanacion.android:id/authorization_btn_authorize");
			el4.click();

			MobileElement el5 = driver.findElementById("com.newspaperdirect.lanacion.android:id/txtSeeAll");
			el5.click();

			for(int i = 1; i <= 2; i++) {
				int check1 = driver.findElementsByXPath("//it.sephiroth.android.library.widget.HListView//android.widget.LinearLayout[" + i + "]//android.widget.RelativeLayout[1]//android.view.View[@resource-id='com.newspaperdirect.lanacion.android:id/viewItem']").size();
				if(check1 >= 0) {
					for(int j = 1; j <= 8; j++) {
						int check2 = driver.findElementsByXPath("//it.sephiroth.android.library.widget.HListView//android.widget.LinearLayout[" + i + "]//android.widget.RelativeLayout[" + j + "]//android.view.View[@resource-id='com.newspaperdirect.lanacion.android:id/viewItem']").size();
						if(check2 >= 0) {
							try {
								MobileElement el6 = driver.findElementByXPath("//it.sephiroth.android.library.widget.HListView//android.widget.LinearLayout[" + i + "]//android.widget.RelativeLayout[" + j + "]//android.view.View[@resource-id='com.newspaperdirect.lanacion.android:id/viewItem']");
								el6.click();
							} catch(Exception e) {
								break;
							}
							String text = null;
							try {
								text = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.newspaperdirect.lanacion.android:id/action_bar']//android.widget.TextView").getText();
							} catch (StaleElementReferenceException e) {
								Thread.sleep(4000);
								text = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.newspaperdirect.lanacion.android:id/action_bar']//android.widget.TextView").getText();
							}

							if(text.equals(keyWord)) {
								log.info("Element found");
								i = 5;
								break;
							}
							driver.navigate().back();
						}
					}
				}
			}

			MobileElement el7 = driver.findElementById("com.newspaperdirect.lanacion.android:id/order_btn_ok");
			el7.click();

			MobileElement el71 = (MobileElement) driver.findElementById("android:id/button1");
			el71.click();

			for(int i = 0; i < 20; i++) {
				int check = driver.findElementsByXPath("//android.widget.Button[@text='OPEN']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Page opened");
					MobileElement el8 = driver.findElementByXPath("//android.widget.Button[@text='OPEN']");
					el8.click();
					break;
				}
			}

			String date = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.newspaperdirect.lanacion.android:id/date']").getText();
			log.info(date);
			
			String images = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.newspaperdirect.lanacion.android:id/pages']").getText();
			
			int imagesCount = Integer.parseInt(images.substring(images.lastIndexOf(" ") + 1));
			log.info(imagesCount);
			
			Thread.sleep(4000);

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}

	}

}
