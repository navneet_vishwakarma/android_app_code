package samsung_download1;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * MT --- program to download MT app publications
 * @author NA20251768
 *
 */
public class MT extends AppRelated {

	/*
Pravo ----- DONE
DNA (Derni�res Nouvelles d'Alsace)-Strasbourg
DNA (Derni�res Nouvelles d'Alsace)-Mulhouse/Haut Rhin Sud
DNA (Derni�res Nouvelles d'Alsace)-Molsheim Schirmeck
Dernieres Nouvelles d'Alsace (Colmar Ried)-Colmar Ried
L'Alsace-Mulhouse
	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1106, 1737, 226, 1737};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(MT.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("com.dna_prod.presse:id/button_agree");
			el1.click();
			MobileElement el2 = driver.findElementById("com.dna_prod.presse:id/button_start");
			el2.click();
			MobileElement el3 = driver.findElementById("com.android.permissioncontroller:id/permission_allow_foreground_only_button");
			el3.click();

			MobileElement els6 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'STRASBOURG')]");
			els6.click();
			MobileElement el4 = driver.findElementById("com.dna_prod.presse:id/front_page_finish_button");
			el4.click();
			MobileElement el5 = driver.findElementById("com.dna_prod.presse:id/accept_button");
			el5.click();
			
			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsByAccessibilityId("Journal").size();
				if(check == 0) {
					log.info("Click Accept button");
					el5 = driver.findElementById("com.dna_prod.presse:id/accept_button");
					el5.click();
				} else {
					break;
				}
			}
			
			MobileElement el6 = driver.findElementByAccessibilityId("Journal");
			el6.click();
			MobileElement el8 = driver.findElementByAccessibilityId("Menu");
			el8.click();
			MobileElement el9 = driver.findElementByAccessibilityId("Mon Compte");
			el9.click();
			MobileElement el10 = driver.findElementById("com.dna_prod.presse:id/email_edittext");
			el10.sendKeys(username());
			MobileElement el11 = driver.findElementById("com.dna_prod.presse:id/password_edittext");
			el11.sendKeys(password());
			MobileElement els7 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'CONNECTE')]");
			els7.click();
			MobileElement el12 = driver.findElementByAccessibilityId("Journal");
			el12.click();
			MobileElement el13 = driver.findElementById("com.dna_prod.presse:id/edition_date_textview");
			el13.click();
			MobileElement el14 = driver.findElementById("com.dna_prod.presse:id/download_button");
			el14.click();
			MobileElement els8 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'LIRE')]");
			els8.click();
			MobileElement el15 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]");
			el15.click();
			MobileElement el16 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[6]/android.widget.TextView");
			el16.click();
			MobileElement el17 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.widget.ImageView");
			el17.click();
			/*
			(new TouchAction(driver))
			  .press({x: 921, y: 473})
			  .moveTo({x: 99: y: 473})
			  .release()
			  .perform()
			  
			MobileElement el18 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.widget.ImageView");
			el18.click();
			(new TouchAction(driver))
			  .press({x: 985, y: 1122})
			  .moveTo({x: 95: y: 1115})
			  .release()
			  .perform()

			  
			  
			  
			  
			
			String date = "30/09/2021";
			MobileElement els17 = driver.findElementByXPath("//android.widget.GridView[@resource-id='cz.alza.media:id/library_list']//android.widget.LinearLayout[@resource-id='cz.alza.media:id/root_item_view'][2]//android.widget.TextView[@resource-id='cz.alza.media:id/name_tv']");
			els17.click();
			
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsById("cz.alza.media:id/glasses_iv").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			
			MobileElement els19 = driver.findElementById("cz.alza.media:id/glasses_iv");
			els19.click();
			//MobileElement els20 = driver.findElementByAccessibilityId(rw.excelReader(app, keyWord, "testData3"));
			MobileElement els20 = driver.findElementByXPath("//android.widget.TextView[@resource-id='cz.alza.media:id/pspdf__menu_option_thumbnail_grid']");

			els20.click();
			
			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.RelativeLayout[last()]";
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
			text1 = text1.substring(text1.indexOf(" ") + 1);
			int count = Integer.parseInt(text1);
			System.out.println("Count 1 = " + count);

			int imagesCount = 0;
			
			if(count != 1) {
				for(int j = 0; j < 10; j++) {
					for(int i = 0; i < 5; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);
					
					String text = null;
					try {
						text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
						text = text.substring(text.indexOf(" ") + 1);
						System.out.println("Count 2 = " + text);
					} catch (NoSuchElementException e) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
					}
					try {
						imagesCount = Integer.parseInt(text);
					} catch (NumberFormatException n) {
						log.info("NumberFormatException in text " + text);
						
						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
						
						Thread.sleep(7000);
						
						text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
						text = text.substring(text.indexOf(" ") + 1);
						System.out.println("Count 3 = " + text);
						imagesCount = Integer.parseInt(text);
					}
					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}
		
			log.info("The images count is " + imagesCount);
			
			els20 = driver.findElementByXPath("//android.widget.TextView[@resource-id='cz.alza.media:id/pspdf__menu_option_thumbnail_grid']");
			els20.click();

			MobileElement els21 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='cz.alza.media:id/pspdf__document_view']//android.view.View[@index=0]");
			els21.click();
			

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
			*/
		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}
}
