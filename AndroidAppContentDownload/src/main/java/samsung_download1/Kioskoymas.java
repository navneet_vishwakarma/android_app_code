package samsung_download1;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Kioskoymas --- program to download Kioskoymas app publications
 * @author NA20251768
 *
 */
public class Kioskoymas extends AppRelated {

	/*

El Comercio ----- FF ----- DONE
La Voz de C�diz ----- FF ----- DONE
----- Heraldo de Arag�n ----- FF ----- DONE
El Correo ----- FF ----- DONE
			Diario de Noticias ----- FV - Sorry, we couldn't find any publication for ""
El Norte de Castilla ----- FF ----- DONE
Palentino ----- FF ----- DONE
Diario de �vila ----- FF ----- DONE
Diario de Burgos ----- FV ----- DONE
Diario de Navarra ----- FF ----- DONE
Ideal ----- FF ----- DONE
Las Provincias ----- FF ----- DONE
El Diario Montanes ----- FV ----- DONE
			El Diario Monta��s - Cantabria en la Mesa ----- FV - Sorry, we couldn't find any publication for ""
			La Verdad de Murcia ----- VV - Sorry, we couldn't find any publication for ""
			La Opinion de murcia ----- VV ----- DONE
La Gaceta de Salamanca ----- FF ----- DONE
Diario de Leon ----- FF ----- DONE
Canarias 7 ----- FF ----- DONE
Diari de Tarragona ----- FF - DONE
Diario de Avisos ----- FF ----- DONE
El Dia ----- FF ----- DONE
			La Voz de Galicia Arousa ----- FF - Sorry, we couldn't find any publication for ""
Lecturas ----- FF ----- DONE

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Kioskoymas.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/tab_more").size();
				if(check == 0) {
					log.info("Loading...");
				} else {
					log.info("Search opened");
					break;
				}
			}

			MobileElement el1 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/tab_more");
			el1.click();
			
			MobileElement el2 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/sign_in_button");
			el2.click();

			MobileElement el6 = driver.findElementByAccessibilityId("authorization_user_name");
			el6.sendKeys(username());

			MobileElement el12 = driver.findElementByAccessibilityId("authorization_user_password");
			el12.sendKeys(password());

			MobileElement el13 = driver.findElementByAccessibilityId("authorization_signin");
			el13.click();
			
			MobileElement el14 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/tab_local_store");
			el14.click();

			Thread.sleep(4000);

			MobileElement el15 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/icon_search");
			el15.click();

			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/search_view").size();
				if(check == 0) {
					MobileElement el90 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/icon_search");
					el90.click();					
				} else {
					log.info("Search opened");
					break;
				}
			}

			MobileElement el16 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/search_view");
			el16.sendKeys(keyWord);
			Thread.sleep(7000);

			MobileElement el17 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/thumbnail");
			el17.click();

			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/title_current_position").size();
				if(check == 0) {
					log.info("Loading...");
					try {
						MobileElement el18 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ImageView");
						el18.click();
					} catch (NoSuchElementException n1) {
						log.info("Even parent not found");
						try {
							driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/txtTip").click();
							break;
						} catch (Exception e) {
							log.info("Tip not found");
						}
					}
					
				} else {
					log.info("Search opened");
					break;
				}
			}

			String date = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/title_current_position").getText();
			log.info(date);

			int imagesCount = Integer.parseInt(date.substring(date.lastIndexOf(" ") + 1));
			log.info("Images count = " + imagesCount);
			
			int checkTip = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/txtTip").size();
			
			if(checkTip != 0) {
				driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/txtTip").click();
			}

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}
}
