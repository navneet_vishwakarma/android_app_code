package headspin_googlePixel3_download1;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * QuotidianoDelSud --- program to download QuotidianoDelSud app publications
 * @author NA20251768
 *
 */
public class QuotidianoDelSud extends AppRelated {

	/*

REGGIO CALABRIA ----- VV ----- DONE
BASILICATA ----- VV ----- DONE
VIBO VALENTIA ----- VV ----- DONE

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1103, 1621, 217, 1621};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(QuotidianoDelSud.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("//android.view.View[@text='Reggio Calabria']//preceding-sibling::android.widget.Image").size();
				if(check == 0) {
					log.info("Loading...");
				} else {
					log.info("Search opened");
					break;
				}
			}
			
			
			MobileElement els3 = driver.findElementByXPath("//android.view.View[@text='Reggio Calabria']//preceding-sibling::android.widget.Image");
			els3.click();
			
			
			
			MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Mio Profilo']");
			els1.click();

			MobileElement els5 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_username']");
			els5.sendKeys(username());

			MobileElement els7 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_password']");
			els7.sendKeys(password());

			MobileElement el2 = driver.findElementByAccessibilityId("ACCEDI");
			el2.click();

			driver.navigate().back();

			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByXPath("//android.view.View[@content-desc='" + keyWord + "']/android.widget.TextView").size();
				if(check == 0) {
					(new AndroidTouchAction(driver)).press(PointOption.point(533, 1338))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(544, 614))
					.release().perform();

					log.info("Scrolled Down...");
				} else {
					log.info("Page opened");
					MobileElement el8 = driver.findElementByXPath("//android.view.View[@content-desc='" + keyWord + "']/android.widget.TextView");
					el8.click();
					break;
				}
			}

			String date = driver.findElementByXPath("//android.view.View[@text='Quotidiano del Sud']//following-sibling::android.view.View[1]/android.view.View[1]").getText();
			date = date.replaceAll("/","_");
			log.info(date);

			MobileElement els11 = driver.findElementByXPath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]");
			els11.click();

			String lastThumbnailXpath = "//android.widget.AdapterView[@index=1]/child::android.widget.RelativeLayout[last()]//android.widget.TextView";
			String secondLastThumbnailXpath = "//android.widget.AdapterView[@index=1]/child::android.widget.RelativeLayout[last()-1]//android.widget.TextView";

			int imagesCount = getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView[1]/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
			el1.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
