package headspin_googlePixel3_download1;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Dergilik --- program to download Dergilik app publications
 * @author NA20251768
 *
 */
public class Dergilik2 extends AppRelated {

	/*
	 * 
Samdan Plus ----- FF ----- DONE

	 */

	static int[] swipeCoordinates = {1065, 924, 132, 924};
	static int[] thumbnailSwipeCoord = {1140, 1772, 148, 1772};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Dergilik2.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			Dergilik.dergilikInitialization(driver, keyWord);

			String date = null;

			int check2 = driver.findElementsByXPath("//android.widget.ScrollView//android.widget.LinearLayout[1]//android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_month']").size();
			if(check2 >= 1) {
				date = driver.findElementByXPath("//android.widget.ScrollView//android.widget.LinearLayout[1]//android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_month']").getText();
				log.info(date);

			} else {
				date = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_size']").getText();
				log.info(date);
			}

			int check3 = driver.findElementsByAccessibilityId(keyWord).size();
			if(check3 >= 1) {
				MobileElement el12 = driver.findElementByAccessibilityId(keyWord);
				el12.click();

			} else {
				MobileElement el12 = driver.findElementById("com.arneca.dergilik.main3x:id/iv_download_image");
				el12.click();
			}

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[last()]//android.widget.ImageView").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			MobileElement el80 = driver.findElementByAccessibilityId("grid view");
			el80.click();

			String text1 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView//android.widget.RelativeLayout[last()]").getAttribute("content-desc");

			int count = Integer.parseInt(text1.substring(5));

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView//android.widget.RelativeLayout[last()]").getAttribute("content-desc");
					imagesCount = Integer.valueOf(text.substring(5));

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}

			log.info("The images count is " + imagesCount);	

			driver.navigate().back();

			MobileElement el13 = driver.findElementByClassName("com.pspdfkit.internal.views.document.DocumentView");
			el13.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}

}
