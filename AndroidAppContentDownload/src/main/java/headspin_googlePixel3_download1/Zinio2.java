package headspin_googlePixel3_download1;

import java.time.Duration;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Zinio --- program to download Zinio app publications
 * @author NA20251768
 *
 */
public class Zinio2 extends AppRelated {

	/*	
 	mediamanagement@prime-research.com, Mainz2014!

	Business Today ----- FF ----- DONE
	Business Weekly ----- FF ----- DONE
	Valeurs Actuelles ----- FF ----- DONE
	Auto Plus France ----- FF ----- DONE

	a2subscriptions@prime-research.com,	Maynard309

	The Economist ----- FF ----- DONE
	Time Magazine ----- FF ----- DONE
	
	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {1093, 1706, 188, 1706};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Zinio2.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]");
			el1.click();

			MobileElement el2 = driver.findElementById("com.zinio.mobile.android.reader:id/btn_start");
			el2.click();

			Thread.sleep(7000);

			MobileElement el3 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Profile\"]/android.view.ViewGroup/android.widget.TextView");
			el3.click();

			MobileElement el4 = driver.findElementByXPath("//android.widget.TextView[@text='Sign In']");
			el4.click();

			MobileElement el5 = driver.findElementByXPath("//android.widget.Button[@text='SIGN IN']");
			el5.click();

			MobileElement el6 = driver.findElementById("com.zinio.mobile.android.reader:id/email_field");
			el6.sendKeys(username());

			MobileElement el7 = driver.findElementById("com.zinio.mobile.android.reader:id/password_field");
			el7.sendKeys(password());

			MobileElement el8 = driver.findElementById("com.zinio.mobile.android.reader:id/sign_in_button");
			el8.click();
			
			MobileElement el81 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Library\"]/android.view.ViewGroup/android.widget.TextView");
			el81.click();
			
			for(int i = 0; i < 100; i ++) {
				//int check = driver.findElementsByXPath("//android.widget.LinearLayout[@content-desc=\"Downloaded\"]/android.widget.TextView").size();
				int check = driver.findElementsByXPath("//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]//android.widget.ImageView[@resource-id='com.zinio.mobile.android.reader:id/iv_cover']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Library opened");
					break;
				}
			}
			
			MobileElement el82 = driver.findElementById("com.zinio.mobile.android.reader:id/search_action");
			el82.click();
			
			MobileElement el83 = driver.findElementById("com.zinio.mobile.android.reader:id/search_src_text");
			el83.clear();
			el83.sendKeys("Invalid");

			Thread.sleep(3000);
			MobileElement el10 = driver.findElementById("com.zinio.mobile.android.reader:id/search_src_text");
			el10.clear();
			el10.sendKeys(keyWord);

			Thread.sleep(5000);

			KeyInput keyboard = new KeyInput("keyboard");
			Sequence sendKeys = new Sequence(keyboard, 0);
			sendKeys.addAction(keyboard.createKeyDown(Keys.ENTER.getCodePoint()));

			driver.perform(Arrays.asList(sendKeys));

			Thread.sleep(5000);

			MobileElement el14 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]//android.widget.ImageView[@resource-id='com.zinio.mobile.android.reader:id/iv_cover']");
			el14.click();

			Thread.sleep(7000);

			String date = "07102021";
			log.info(date);

			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@resource-id='com.zinio.mobile.android.reader:id/pages_portrait_container'][last()]//android.widget.TextView[@resource-id='com.zinio.mobile.android.reader:id/tv_folio_number']";

			String secondlastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@resource-id='com.zinio.mobile.android.reader:id/pages_portrait_container'][last()-1]//android.widget.TextView[@resource-id='com.zinio.mobile.android.reader:id/tv_folio_number']";

			/*
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='PDF']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					MobileElement el16 = driver.findElementByXPath("//android.widget.TextView[@text='PDF']");
					el16.click();
					break;
				}
			}
			*/

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath(lastThumbnailXpath).size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			String count = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			String textCount = null;


			if(!count.equals("C4")) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					textCount = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

					if(textCount.equals(count))
						break;
					else count = textCount;
				}
			}

			String text = driver.findElementByXPath(secondlastThumbnailXpath).getAttribute("text");

			int imagesCount = Integer.parseInt(text);

			log.info("The images count is " + imagesCount + 1);

			MobileElement el12 = driver.findElementById("com.zinio.mobile.android.reader:id/pdf_reader_container_coordinator");
			el12.click();

			tearDown(imagesCount + 1, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
