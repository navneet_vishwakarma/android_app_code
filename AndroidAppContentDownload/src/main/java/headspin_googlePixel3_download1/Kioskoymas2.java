package headspin_googlePixel3_download1;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Kioskoymas --- program to download Kioskoymas app publications
 * @author NA20251768
 *
 */
public class Kioskoymas2 extends AppRelated {

	/*
			El Correo (Araba/�lava) ----- FV ----- DONE

			Ideal (Almer�a) ----- FV ----- DONE

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Kioskoymas2.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/tab_more").size();
				if(check == 0) {
					log.info("Loading...");
				} else {
					log.info("Search opened");
					break;
				}
			}

			MobileElement el1 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/tab_more");
			el1.click();

			MobileElement el4 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/sign_in_button");
			el4.click();

			MobileElement el6 = driver.findElementByAccessibilityId("authorization_user_name");
			el6.sendKeys(username());

			MobileElement el12 = driver.findElementByAccessibilityId("authorization_user_password");
			el12.sendKeys(password());

			MobileElement el13 = driver.findElementByAccessibilityId("authorization_signin");
			el13.click();
			MobileElement el14 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.ImageView");
			el14.click();

			Thread.sleep(4000);

			MobileElement el15 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/icon_search");
			el15.click();

			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/search_view").size();
				if(check == 0) {
					MobileElement el90 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/icon_search");
					el90.click();					
				} else {
					log.info("Search opened");
					break;
				}
			}

			MobileElement el16 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/search_view");
			el16.sendKeys(rw.excelReader(app, keyWord, "testData2"));
			Thread.sleep(7000);

			MobileElement el17 = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/thumbnail");
			el17.click();

			MobileElement el161 = driver.findElementByAccessibilityId("Editions");
			el161.click();

			MobileElement els162 = driver.findElementByXPath("//android.widget.TextView[@text='"+ rw.excelReader(app, keyWord, "testData1") +"']");
			els162.click();


			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/title_current_position").size();
				if(check == 0) {
					log.info("Loading...");
					try {
						MobileElement el18 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ImageView");
						el18.click();
					} catch (NoSuchElementException n1) {
						log.info("Even parent not found");
						try {
							driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/txtTip").click();
							break;
						} catch (Exception e) {
							log.info("Tip not found");
						}
					}
					
				} else {
					log.info("Search opened");
					break;
				}
			}

			String date = driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/title_current_position").getText();
			log.info(date);

			int imagesCount = Integer.parseInt(date.substring(date.lastIndexOf(" ") + 1));

			int checkTip = driver.findElementsById("com.newspaperdirect.kioskoymas.android.hc:id/txtTip").size();
			
			if(checkTip != 0) {
				driver.findElementById("com.newspaperdirect.kioskoymas.android.hc:id/txtTip").click();
			}
			
			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}

}
