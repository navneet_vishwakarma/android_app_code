package headspin_googlePixel3_download1;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Bergamo --- program to download Bergamo app publications
 * @author NA20251768
 *
 */
public class Bergamo extends AppRelated {

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1143, 1759, 207, 1759};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Bergamo.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			for(int i = 0; i<5; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='Mio Profilo']").size();
				if(check!=0) {
					log.info("Loading");
				} else {
					log.info("Element found");
					Thread.sleep(3000);
					break;
				}
			}

			MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Mio Profilo']");
			els1.click();

			MobileElement els3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_username']");
			els3.sendKeys(username());

			MobileElement els4 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_password']");
			els4.sendKeys(password());

			MobileElement el2 = driver.findElementByXPath("//android.view.View[@text='Accedi']");
			el2.click();

			for(int i = 0; i<5; i++) {
				int check = driver.findElementsByXPath("//android.view.View[@text='SCOLLEGA IL TUO DISPOSITIVO']").size();
				if(check!=0) {
					log.info("Loading");
				} else {
					log.info("Element found");
					Thread.sleep(3000);
					break;
				}
			}

			MobileElement els5 = driver.findElementByXPath("//android.widget.TextView[@text='Home']");
			els5.click();

			String download = driver.findElementByXPath("//android.widget.Gallery/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView").getText();
			if(download.equals("ANTEPRIMA")) {
				(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
				.release().perform();
				Thread.sleep(3000);
			}

			String date = driver.findElementByXPath("//android.widget.TextView[@text='SCARICA']/../preceding-sibling::android.widget.TextView").getText();
			date = date.replaceAll("/","_");
			log.info(date);

			MobileElement el4 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Gallery/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.ImageView");
			el4.click();

			MobileElement el41 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
			el41.click();

			MobileElement els11 = driver.findElementByXPath("//android.widget.TextView[@text='Pagine']");
			els11.click();

			String lastThumbnailXpath = "//android.widget.AdapterView[@index=1]/child::android.widget.RelativeLayout[last()]//android.widget.TextView";
			String secondLastThumbnailXpath = "//android.widget.AdapterView[@index=1]/child::android.widget.RelativeLayout[last()-1]//android.widget.TextView";

			int imagesCount = AppRelated.getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el22 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView[1]/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
			el22.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
