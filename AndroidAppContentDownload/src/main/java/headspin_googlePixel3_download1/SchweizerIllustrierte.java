package headspin_googlePixel3_download1;

import java.time.Duration;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * SchweizerIllustrierte --- program to download SchweizerIllustrierte app publications
 * @author NA20251768
 *
 */
public class SchweizerIllustrierte extends AppRelated {

	/*

IlCentro ----- VV

	 */

	static int[] swipeCoordinates = {1006, 1031, 78, 1045};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(SchweizerIllustrierte.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			Thread.sleep(10000);
			String date = driver.findElementById("com.threedflip.schweizerillustrierte:id/new_storefront_title").getText();
			log.info(date);

			Thread.sleep(4000);
			MobileElement el1 = driver.findElementById("com.threedflip.schweizerillustrierte:id/new_storefront_epaper_cover");
			el1.click();

			Thread.sleep(4000);
			MobileElement el3 = driver.findElementById("com.threedflip.schweizerillustrierte:id/magazine_toolbar_index_button");
			el3.click();

			Thread.sleep(4000);
			MobileElement el4 = driver.findElementById("com.threedflip.schweizerillustrierte:id/index_header_pages_button");
			el4.click();

			int imagesCountStart = 1;
			int imagesCount = 1;
			for(int i = 0; i < 500; i++) {
				for(int j = 1; j <= 5; j++) {

					String xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.threedflip.schweizerillustrierte:id/magazine_toolbar_pages_items']//android.widget.LinearLayout[" + j + "]//android.widget.LinearLayout[last()]//android.widget.TextView[@resource-id='com.threedflip.schweizerillustrierte:id/thumbnail_text_right']";
					int size = driver.findElements(By.xpath(xpath)).size();
					if(size == 0)
						break;
					String text = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView[@resource-id='com.threedflip.schweizerillustrierte:id/magazine_toolbar_pages_items']//android.widget.LinearLayout[" + j + "]//android.widget.LinearLayout[last()]//android.widget.TextView[@resource-id='com.threedflip.schweizerillustrierte:id/thumbnail_text_right']").getText();
					imagesCount = Integer.parseInt(text);
					log.info(imagesCount);
				}
				imagesCountStart = imagesCount;

				for(int k = 0; k < 3; k++) {
					(new AndroidTouchAction(driver)).press(PointOption.point(533, 1338))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(544, 614))
					.release().perform();
				}

				log.info("Scrolled Down...");

				Thread.sleep(5000);

				String text = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView[@resource-id='com.threedflip.schweizerillustrierte:id/magazine_toolbar_pages_items']//android.widget.LinearLayout[1]//android.widget.LinearLayout[last()]//android.widget.TextView[@resource-id='com.threedflip.schweizerillustrierte:id/thumbnail_text_right']").getText();
				imagesCount = Integer.parseInt(text);
				log.info(imagesCount);
				if(imagesCount <= imagesCountStart)
					break;
			}

			log.info("Images imagesCount = " + imagesCount);

			el3 = driver.findElementById("com.threedflip.schweizerillustrierte:id/magazine_toolbar_index_button");
			el3.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}
}
