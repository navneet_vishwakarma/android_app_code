package headspin_googlePixel3_download1;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * IlRomanista --- program to download IlRomanista app publications
 * @author NA20251768
 *
 */
public class IlRomanista extends AppRelated {

	// DONE

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1134, 1555, 254, 1555};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(IlRomanista.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("//android.widget.Button").size();
				if(check == 0) {
					log.info("Loading...");
				} else {
					log.info("Search opened");
					Thread.sleep(10000);
					break;
				}
			}

			MobileElement els1 = driver.findElementByXPath("//android.widget.Button");
			els1.click();
			
			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsByXPath("//android.view.View[contains(@text,'Accedi')]").size();
				if(check == 0) {
					log.info("Click again...");
					els1 = driver.findElementByXPath("//android.widget.Button");
					els1.click();
				} else {
					log.info("Search opened");
					break;
				}
			}

			MobileElement el1 = driver.findElementByXPath("//android.view.View[contains(@text,'Accedi')]");
			el1.click();

			MobileElement els2 = driver.findElementByXPath("//android.widget.EditText[@resource-id='email']");
			els2.sendKeys(username());

			MobileElement els3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='password']");
			els3.sendKeys(password());

			MobileElement els4 = driver.findElementByXPath("//android.widget.Button[@text='ACCEDI']");
			els4.click();

			driver.navigate().back();

			String date = driver.findElementByXPath("//android.view.View[@text='Il Romansita']//following-sibling::android.view.View[1]/android.view.View[1]").getText();
			date = date.replaceAll("/","_");
			log.info(date);
			
			MobileElement els41 = driver.findElementByXPath("//android.view.View[@text='Il Romansita']//following-sibling::android.view.View[1]/android.view.View[1]");
			els41.click();

			MobileElement els11 = driver.findElementByXPath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]");
			els11.click();

			String lastThumbnailXpath = "//com.miles33.vnp2.TwoWayView//android.widget.RelativeLayout[last()]//android.widget.TextView";
			String secondLastThumbnailXpath = "//com.miles33.vnp2.TwoWayView//android.widget.RelativeLayout[last()-1]//android.widget.TextView";

			int imagesCount = getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el7 = driver.findElementByXPath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]");
			el7.click();

			MobileElement el8 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
			el8.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
