package headspin_googlePixel3_download1;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Alza --- program to download Alza app publications
 * @author NA20251768
 *
 */
public class DNA extends AppRelated {

	/*
Pravo ----- DONE
DNA (Derni�res Nouvelles d'Alsace)-Strasbourg
DNA (Derni�res Nouvelles d'Alsace)-Mulhouse/Haut Rhin Sud
DNA (Derni�res Nouvelles d'Alsace)-Molsheim Schirmeck
Dernieres Nouvelles d'Alsace (Colmar Ried)-Colmar Ried
	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {1150, 261, 207, 261};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(DNA.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("com.dna_prod.presse:id/button_agree");
			el1.click();
			Thread.sleep(3000);
			MobileElement el2 = driver.findElementById("com.dna_prod.presse:id/button_start");
			el2.click();
			Thread.sleep(3000);
			MobileElement el3 = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
			el3.click();

			MobileElement els6 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'" + keyWord + "')]");
			els6.click();
			MobileElement el4 = driver.findElementById("com.dna_prod.presse:id/front_page_finish_button");
			el4.click();
			MobileElement el5 = driver.findElementById("com.dna_prod.presse:id/accept_button");
			el5.click();
			
			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsByAccessibilityId("Journal").size();
				if(check == 0) {
					log.info("Click Accept button");
					el5 = driver.findElementById("com.dna_prod.presse:id/accept_button");
					el5.click();
				} else {
					break;
				}
			}
			
			MobileElement el7 = driver.findElementByAccessibilityId("Menu");
			el7.click();
			MobileElement el8 = driver.findElementByAccessibilityId("Mon Compte");
			el8.click();
			MobileElement el9 = driver.findElementById("com.dna_prod.presse:id/email_edittext");
			el9.sendKeys(username());
			MobileElement el10 = driver.findElementById("com.dna_prod.presse:id/password_edittext");
			el10.sendKeys(password());
			MobileElement els2 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'JE ME CONNECTE')]");
			els2.click();
			Thread.sleep(5000);
			MobileElement el11 = driver.findElementByAccessibilityId("Journal");
			el11.click();

			MobileElement els5 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView[@resource-id='com.dna_prod.presse:id/catalog_recyclerview']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.dna_prod.presse:id/edition_date_textview']");
			String date = els5.getText();
			log.info(date);
			
			MobileElement el12 = driver.findElementById("com.dna_prod.presse:id/download_button");
			el12.click();
			
			for(int i = 0; i < 20; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[contains(@text,'Lire')]").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			MobileElement els11 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'Lire')]");
			els11.click();
			
			MobileElement els8 = driver.findElementByXPath("//android.widget.ImageView/parent::android.widget.FrameLayout[@index=1]");
			els8.click();
			
			String lastThumbnailXpath = "//android.widget.FrameLayout/preceding-sibling::androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[last()]//android.widget.TextView";
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
					imagesCount = Integer.parseInt(text);

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}
			
			log.info("The images count is " + imagesCount);
			
			MobileElement els13 = driver.findElementByXPath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]//android.widget.ImageView/parent::android.widget.FrameLayout[@index=1]");
			els13.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
