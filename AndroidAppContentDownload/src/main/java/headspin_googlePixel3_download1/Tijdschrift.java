package headspin_googlePixel3_download1;

import java.time.Duration;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Tijdschrift --- program to download Tijdschrift app publications
 * @author NA20251768
 *
 */
public class Tijdschrift extends AppRelated {

	/*
	Flair ----- FF ----- DONE
	Veronica ----- FF ----- DONE
	Grazia ----- FF ----- DONE
	Libelle ----- FF ----- DONE
	Viva [NL] ----- FF ----- DONE
	Autoweek ----- FF ----- DONE
	Nieuwe Revu ----- FF ----- DONE
	Panorama ----- FF ----- DONE
	Margriet ----- FF ----- Purchase Issue

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1109, 1706, 236, 1706};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Tijdschrift.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsById("nl.tijdschrift.newsstand:id/btn_start").size();
				if(check == 0) {
					log.info("Loaded Again...");
				} else {
					log.info("Page opened");
					MobileElement els1 = driver.findElementById("nl.tijdschrift.newsstand:id/btn_start");
					els1.click();
					break;
				}
			}

			MobileElement el3 = driver.findElementById("nl.tijdschrift.newsstand:id/view_auth_button_b");
			el3.click();

			MobileElement el6 = driver.findElementById("nl.tijdschrift.newsstand:id/email_field");
			el6.sendKeys(username());

			MobileElement el7 = driver.findElementById("nl.tijdschrift.newsstand:id/password_field");
			el7.sendKeys(password());

			MobileElement el8 = driver.findElementById("nl.tijdschrift.newsstand:id/sign_in_button");
			el8.click();

			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByAccessibilityId("close_button").size();
				if(check == 0) {
					log.info("Loaded Again...");
				} else {
					log.info("Page opened");
					driver.navigate().back();
					break;
				}
			}

			MobileElement el9 = driver.findElementById("nl.tijdschrift.newsstand:id/menu_search");
			el9.click();
			
			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsById("nl.tijdschrift.newsstand:id/search_src_text").size();
				if(check == 0) {
					el9 = driver.findElementById("nl.tijdschrift.newsstand:id/menu_search");
					el9.click();
				} else {
					break;
				}
			}
			
			MobileElement el10 = driver.findElementById("nl.tijdschrift.newsstand:id/search_src_text");
			el10.sendKeys(keyWord);
			el10.click();

			Thread.sleep(5000);

			KeyInput keyboard = new KeyInput("keyboard");
			Sequence sendKeys = new Sequence(keyboard, 0);
			sendKeys.addAction(keyboard.createKeyDown(Keys.ENTER.getCodePoint()));
			//sendKeys.addAction(keyboard.createKeyUp(Keys.ENTER.getCodePoint()));

			driver.perform(Arrays.asList(sendKeys));

			Thread.sleep(5000);

			MobileElement el14 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[1]/android.view.ViewGroup/android.widget.TextView[1]");
			el14.click();

			Thread.sleep(7000);

			String date = driver.findElementById("nl.tijdschrift.newsstand:id/issue_name").getText();
			log.info(date);

			MobileElement el15 = driver.findElementById("nl.tijdschrift.newsstand:id/single_issue_button");
			el15.click();

			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@resource-id='nl.tijdschrift.newsstand:id/pages_portrait_container'][last()]//android.widget.TextView[@resource-id='nl.tijdschrift.newsstand:id/tv_folio_number']";

			String secondlastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@resource-id='nl.tijdschrift.newsstand:id/pages_portrait_container'][last()-1]//android.widget.TextView[@resource-id='nl.tijdschrift.newsstand:id/tv_folio_number']";

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath(lastThumbnailXpath).size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			String count = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			String textCount = null;


			if(!count.equals("C4")) {
				for(int j = 0; j < 5; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					textCount = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

					if(textCount.equals(count))
						break;
					else count = textCount;
				}
			}

			String text = driver.findElementByXPath(secondlastThumbnailXpath).getAttribute("text");

			int imagesCount = Integer.parseInt(text);

			log.info("The images count is " + (imagesCount + 1));

			MobileElement el12 = driver.findElementById("nl.tijdschrift.newsstand:id/pdf_reader_container_coordinator");
			el12.click();

			tearDown(imagesCount + 1, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
