package headspin_googlePixel3_download1;

import java.io.File;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;
import utility.ImagesPDFrelated;
import utility.ReadWrite;

/**
 * Aamulehti --- program to download Aamulehti app publications
 * @author NA20251768
 *
 */
public class Aamulehti extends AppRelated {

	/*
Aamulehti ----- DONE

	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Aamulehti.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			MobileElement els1 = driver.findElementByXPath("//android.widget.Button[@text='OK']");
			els1.click();
			Thread.sleep(2000);
			MobileElement els2 = driver.findElementByXPath("//android.widget.TextView[@text='Vahvista']");
			els2.click();
			Thread.sleep(2000);
			MobileElement el1 = driver.findElementById("com.almamedia.kioski.aamulehti:id/ok");
			el1.click();
			MobileElement el2 = driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
			el2.click();
			MobileElement el3 = driver.findElementById("com.almamedia.kioski.aamulehti:id/login");
			el3.click();
			MobileElement el4 = driver.findElementByXPath("//android.widget.EditText[@resource-id='username']");
			el4.sendKeys(username());
			MobileElement el5 = driver.findElementByXPath("//android.widget.EditText[@resource-id='password']");
			el5.sendKeys(password());
			MobileElement els3 = driver.findElementByXPath("//android.widget.Button[@text='Kirjaudu']");
			els3.click();

			MobileElement els13 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.almamedia.kioski.aamulehti:id/swipeRefresh']//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.almamedia.kioski.aamulehti:id/dateView']");
			String date = els13.getText().replaceAll(".", "/");
			System.out.println(date);
			els13.click();
			
			MobileElement els14 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.almamedia.kioski.aamulehti:id/swipeRefresh']//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.almamedia.kioski.aamulehti:id/image']");
						
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByAccessibilityId("Grid TOC").size();
				if(check == 0) {
					log.info("Loading");
					els14 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.almamedia.kioski.aamulehti:id/swipeRefresh']//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.almamedia.kioski.aamulehti:id/image']");
					els14.click();
				} else {
					break;
				}
			}

			img = new ImagesPDFrelated();

			img.deleteFiles(app, keyWord);

			Thread.sleep(10000);

			int imagesCount = 2;
			
			rw = new ReadWrite();
			TakesScreenshot sc = driver;
			File src = sc.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P1.png"));
			log.info(1 + " screenshot taken");
			
			while(imagesCount < 1000) {
				if(imagesCount%2 == 0) {
					try {
						driver.findElementByXPath("//*[@resource-id='com.almamedia.kioski.aamulehti:id/maggioReaderSpreadsPager']/android.view.ViewGroup[@index = 0]");
					} catch (Exception e) {
						(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
						.release().perform();
						
						Thread.sleep(2000);
						try {
							driver.findElementByXPath("//*[@resource-id='com.almamedia.kioski.aamulehti:id/maggioReaderSpreadsPager']/android.view.ViewGroup[@index = 0]");
							
						} catch (Exception e1) {
							break;
						}
					}
				} else {
					try {
						driver.findElementByXPath("//*[@resource-id='com.almamedia.kioski.aamulehti:id/maggioReaderSpreadsPager']/android.view.ViewGroup[@index = 1]");
					} catch (Exception e) {
						(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
						.release().perform();
						
						Thread.sleep(2000);
						try {
							driver.findElementByXPath("//*[@resource-id='com.almamedia.kioski.aamulehti:id/maggioReaderSpreadsPager']/android.view.ViewGroup[@index = 1]");
						} catch (Exception e1) {
							break;
						}
					}
				}
				
				(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
				.release().perform();
				
				Thread.sleep(2000);

				sc = driver;
				src = sc.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P" + imagesCount +".png"));
				log.info(imagesCount + " screenshot taken");
				imagesCount++;
			}

			img.generatePDF(date, keyWord, app);
			
			log.info("The images count is " + imagesCount);
			
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
