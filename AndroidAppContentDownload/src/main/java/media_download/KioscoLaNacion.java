package media_download;

import org.apache.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * KioscoLaNacion --- program to download KioscoLaNacion app publications
 * @author NA20251768
 *
 */
public class KioscoLaNacion extends AppRelated {

	/*

HOLA ----- FF ----- DONE

	 */

	static int[] swipeCoordinates = {999, 1016, 92, 1016};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(KioscoLaNacion.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("com.newspaperdirect.lanacion.android:id/image");
			el1.click();

			MobileElement el2 = driver.findElementById("com.newspaperdirect.lanacion.android:id/authorization_user_name");
			el2.sendKeys(username());

			MobileElement el3 = driver.findElementById("com.newspaperdirect.lanacion.android:id/authorization_password");
			el3.sendKeys(password());

			MobileElement el4 = driver.findElementById("com.newspaperdirect.lanacion.android:id/authorization_btn_authorize");
			el4.click();

			MobileElement el5 = driver.findElementById("com.newspaperdirect.lanacion.android:id/txtSeeAll");
			el5.click();

			for(int i = 1; i <= 4; i++) {
				int check1 = driver.findElementsByXPath("//android.widget.ListView//android.widget.LinearLayout[" + i + "]//android.widget.RelativeLayout[1]//android.view.View[@resource-id='com.newspaperdirect.lanacion.android:id/viewItem']").size();
				if(check1 >= 0) {
					for(int j = 1; j <= 2; j++) {
						int check2 = driver.findElementsByXPath("//android.widget.ListView//android.widget.LinearLayout[" + i + "]//android.widget.RelativeLayout[" + j + "]//android.view.View[@resource-id='com.newspaperdirect.lanacion.android:id/viewItem']").size();
						if(check2 >= 0) {
							MobileElement el6 = driver.findElementByXPath("//android.widget.ListView//android.widget.LinearLayout[" + i + "]//android.widget.RelativeLayout[" + j + "]//android.view.View[@resource-id='com.newspaperdirect.lanacion.android:id/viewItem']");
							el6.click();
							String text = null;
							try {
								text = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.newspaperdirect.lanacion.android:id/action_bar']//android.widget.TextView").getText();
							} catch (StaleElementReferenceException e) {
								Thread.sleep(4000);
								text = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.newspaperdirect.lanacion.android:id/action_bar']//android.widget.TextView").getText();
							}

							if(text.equals(keyWord)) {
								log.info("Element found");
								i = 5;
								break;
							}
							driver.navigate().back();
						}
					}
				}
			}

			MobileElement el7 = driver.findElementById("com.newspaperdirect.lanacion.android:id/order_btn_ok");
			el7.click();

			MobileElement el71 = (MobileElement) driver.findElementById("android:id/button1");
			el71.click();

			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByXPath("//android.widget.Button[@text='OPEN']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Page opened");
					MobileElement el8 = driver.findElementByXPath("//android.widget.Button[@text='OPEN']");
					el8.click();
					break;
				}
			}

			String date = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.newspaperdirect.lanacion.android:id/action_bar']//android.widget.TextView[@index=2]").getText();
			log.info(date);

			int imagesCount = Integer.parseInt(date.substring(date.lastIndexOf(" ") + 1));

			Thread.sleep(4000);

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}

}
