package media_download;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;

import java.io.IOException;
import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Dergilik --- program to download Dergilik app publications
 * @author NA20251768
 *
 */
public class Dergilik extends AppRelated {

	/*

Fanatik ----- VV ----- DONE
Hürriyet Akdeniz ----- VV ----- DONE
Milliyet Ege ----- VV ----- DONE
Posta İzmir ----- VV ---- Unable to search, need video
Yeni Asir ----- VV ----- DONE
Posta ----- FF ----- DONE
Cumhuriyet ----- FF ----- DONE
Samdan Plus ----- FF ----- DONE
Alem ----- FF ----- DONE

	 */

	static int[] swipeCoordinates = {886, 1867, 254, 1867};
	static int[] thumbnailSwipeCoord = {780, 1864, 321, 1864};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Dergilik.class.getName());

	public static void dergilikInitialization(AppiumDriver<MobileElement> driver, String keyWord) throws IOException, InterruptedException {

		MobileElement allow = driver.findElementById("com.android.permissioncontroller:id/permission_allow_button");
		allow.click();

		MobileElement el1 = driver.findElementById("com.arneca.dergilik.main3x:id/iv_esc");
		el1.click();

		Thread.sleep(7000);

		MobileElement el2 = driver.findElementByXPath("//android.widget.ImageView[@content-desc='Profile menu']");		
		el2.click();

		Thread.sleep(7000);
		(new AndroidTouchAction(driver)).tap(point(769, 349)).waitAction(waitOptions(Duration.ofMillis(250))).perform();

		MobileElement el3 = driver.findElementByAccessibilityId("Fast Login");
		el3.click();

		Thread.sleep(3000);
		MobileElement el4 = driver.findElementByXPath("//android.view.View[@resource-id='selectEmailLoginTabLink']");
		el4.click();

		Thread.sleep(3000);
		MobileElement el41 = driver.findElementByXPath("//android.view.View[@resource-id='selectEmailLoginTabLink']");
		el41.click();

		Thread.sleep(3000);

		MobileElement el5 = driver.findElementByXPath("//android.widget.EditText[@resource-id='email']");
		el5.sendKeys(username());

		MobileElement el6 = driver.findElementByXPath("//android.widget.Button[@resource-id='webLogin-button']");
		el6.click();

		Thread.sleep(7000);

		MobileElement el7 = driver.findElementByXPath("//android.widget.EditText[@resource-id='password']");
		el7.sendKeys(password());

		MobileElement el8 = driver.findElementByXPath("//android.widget.Button[@resource-id='password-login-forward-button']");
		el8.click();

		Thread.sleep(4000);

		MobileElement el9 = driver.findElementByAccessibilityId("enter search term");
		el9.click();

		for(int i = 0; i < 4; i ++) {
			int check = driver.findElementsById("com.arneca.dergilik.main3x:id/et_search").size();
			if(check == 0) {
				MobileElement el90 = driver.findElementByAccessibilityId("enter search term");
				el90.click();
			} else {
				log.info("Search opened");
				break;
			}
		}	

		MobileElement el10 = driver.findElementById("com.arneca.dergilik.main3x:id/et_search");
		el10.sendKeys(keyWord);

		MobileElement el11 = driver.findElementByXPath("//android.widget.ListView//android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.arneca.dergilik.main3x:id/iv_image1']");
		el11.click();
	}

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			dergilikInitialization(driver, keyWord);

			String date = null;

			int check2 = driver.findElementsByXPath("//android.widget.ScrollView//android.widget.LinearLayout[1]//android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_month']").size();
			if(check2 >= 1) {
				date = driver.findElementByXPath("//android.widget.ScrollView//android.widget.LinearLayout[1]//android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_month']").getText();
				log.info(date);

			} else {
				date = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_size']").getText();
				log.info(date);
			}

			if(checkOnlyDate(date)) {
				int check3 = driver.findElementsByAccessibilityId(keyWord).size();
				if(check3 >= 1) {
					MobileElement el12 = driver.findElementByAccessibilityId(keyWord);
					el12.click();

				} else {
					MobileElement el12 = driver.findElementById("com.arneca.dergilik.main3x:id/iv_download_image");
					el12.click();
				}

				String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[last()]//android.widget.ImageView";
				for(int i = 0; i < 7; i ++) {
					int check = driver.findElementsByXPath(lastThumbnailXpath).size();
					if(check == 0) {
						log.info("Loaded again...");
					} else {
						log.info("Image opened");
						break;
					}
				}

				String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");

				int count = Integer.parseInt(text1.substring(5));

				int imagesCount = 0;

				if(count != 1) {
					for(int j = 0; j < 20; j++) {
						for(int i = 0; i < 5; i++) {

							(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
							.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
							.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
							.release().perform();

						}

						Thread.sleep(5000);

						String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
						imagesCount = Integer.valueOf(text.substring(5));

						if(imagesCount == count)
							break;
						else count = imagesCount;
					}
				}

				log.info("The images count is " + imagesCount);		

				MobileElement el13 = driver.findElementByClassName("com.pspdfkit.internal.views.document.DocumentView");
				el13.click();

				tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
			} else {
				noMediaFound();
			}
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
