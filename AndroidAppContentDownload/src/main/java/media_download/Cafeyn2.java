package media_download;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Cafeyn --- program to download Cafeyn app publications
 * @author NA20251768
 *
 */
public class Cafeyn2 extends AppRelated {

	/*

Challenges ----- FF ----- DONE
Marianne  ----- FF ----- DONE
Strat�gies ----- FF ----- DONE
Public ----- FF ----- DONE
Telecable Sat Hebdo ----- FF ----- DONE
Le Maine Libre ----- VV ----- DONE
Nord Littoral ----- VF ----- DONE
Var matin ----- VF ----- DONE
Presse Oc�an - Nantes ----- VV ----- DONE
Le Courrier de L'Ouest ----- VV ----- DONE
La Gazette du Val d�OIse ----- VV ----- DONE
La Marseillaise ----- VV ----- DONE
Les Inrockuptibles ----- VV ----- DONE

Paris Normandie ----- DONE
La Voix du Nord - Lille ----- DONE

	 */

	static int[] swipeCoordinates = {1002, 999, 46, 999};
	static int[] thumbnailSwipeCoord = {956, 1754, 124, 1758};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Cafeyn2.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			driver.hideKeyboard();

			MobileElement el1 = driver.findElementById("fr.lekiosque:id/first_screen_connect_button");
			el1.click();

			MobileElement el2 = driver.findElementById("fr.lekiosque:id/signin_first_fragment_btn_email");
			el2.click();

			MobileElement el3 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText");
			el3.sendKeys(username());

			MobileElement el6 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView");
			el6.click();

			MobileElement el7 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText");
			el7.sendKeys(password());

			MobileElement el8 = driver.findElementById("fr.lekiosque:id/signin_email_next_button");
			el8.click();

			MobileElement el9 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Search\"]/android.widget.ImageView");
			el9.click();

			MobileElement el11 = driver.findElementById("fr.lekiosque:id/category_search_bar_text");
			el11.click();

			Thread.sleep(4000);

			MobileElement el12 = driver.findElementById("fr.lekiosque:id/search_bar_edit_text");
			el12.sendKeys(keyWord);
			
			driver.hideKeyboard();

			MobileElement el13 = driver.findElementById("fr.lekiosque:id/issue_cell_cover_image_view");
			el13.click();

			//driver.hideKeyboard();

			String date = driver.findElementById("fr.lekiosque:id/issue_product_release_date").getText();
			log.info(date);

			MobileElement el15 = driver.findElementById("fr.lekiosque:id/issue_product_cover_image_view");
			el15.click();

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.AdapterView/android.view.ViewGroup/android.widget.ImageView").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			
			try {
				MobileElement el151 = driver.findElementById("fr.lekiosque:id/tuto_btn");
				el151.click();
				Thread.sleep(10000);
			} catch (Exception e) {
				log.info("Start Reading not found");
			}
			
			MobileElement el16 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.AdapterView/android.view.ViewGroup/android.widget.ImageView");
			el16.click();  

			String lastThumbnailXpath = "//fr.lekiosque.reader.view.LKThumbsView//android.widget.LinearLayout[@resource-id='fr.lekiosque:id/PreviewPageLinearLayout'][last()]//android.widget.TextView[@resource-id='fr.lekiosque:id/PreviewPageNumber']";
			String secondLastThumbnailXpath = "//fr.lekiosque.reader.view.LKThumbsView//android.widget.LinearLayout[@resource-id='fr.lekiosque:id/PreviewPageLinearLayout'][last()-1]//android.widget.TextView[@resource-id='fr.lekiosque:id/PreviewPageNumber']";
			
			int imagesCount = AppRelated.getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el17 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.FrameLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.AdapterView/android.view.ViewGroup/android.widget.ImageView");
			el17.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
