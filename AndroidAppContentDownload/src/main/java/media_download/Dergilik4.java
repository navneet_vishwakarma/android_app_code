package media_download;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Dergilik --- program to download Dergilik app publications
 * @author NA20251768
 *
 */
public class Dergilik4 extends AppRelated {

	/*

Hürriyet Akdeniz ----- VV ----- DONE

	 */

	static int[] swipeCoordinates = {1038, 953, 35, 953};
	static int[] thumbnailSwipeCoord = {780, 1864, 321, 1864};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Dergilik4.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			Dergilik.dergilikInitialization(driver, keyWord);

			//MobileElement el30 = driver.findElementByAccessibilityId("5 August 2021");
			MobileElement el30 = driver.findElementByXPath("//*[@resource-id = 'com.arneca.dergilik.main3x:id/rv_previous']//android.widget.RelativeLayout[1]");
			el30.click();

			Thread.sleep(5000);

			String date = null;

			int check2 = driver.findElementsByXPath("//android.widget.ScrollView//android.widget.LinearLayout[1]//android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_month']").size();

			if(check2 >= 1) {
				date = driver.findElementByXPath("//android.widget.ScrollView//android.widget.LinearLayout[1]//android.widget.FrameLayout[1]//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_month']").getText();
				log.info(date);

			} else {
				date = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.arneca.dergilik.main3x:id/tv_size']").getText();
				log.info(date);
			}

			int check3 = driver.findElementsByAccessibilityId("Hürriyet").size();
			if(check3 >= 1) {
				MobileElement el12 = driver.findElementByAccessibilityId("Hürriyet");
				el12.click();

			} else {
				MobileElement el12 = driver.findElementById("com.arneca.dergilik.main3x:id/iv_download_image");
				el12.click();
			}

			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[last()]//android.widget.ImageView";

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath(lastThumbnailXpath).size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");

			int count = Integer.parseInt(text1.substring(5));

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
					imagesCount = Integer.parseInt(text.substring(5));

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}

			log.info("The images count is " + imagesCount);		

			MobileElement el13 = driver.findElementByClassName("com.pspdfkit.internal.views.document.DocumentView");
			el13.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
