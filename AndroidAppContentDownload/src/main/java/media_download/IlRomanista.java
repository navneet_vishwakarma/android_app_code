package media_download;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * IlRomanista --- program to download IlRomanista app publications
 * @author NA20251768
 *
 */
public class IlRomanista extends AppRelated {

	// DONE

	static int[] swipeCoordinates = {985, 999, 92, 999};
	static int[] thumbnailSwipeCoord = {1024, 1581, 67, 1592};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(IlRomanista.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement els1 = driver.findElementByXPath("//android.widget.Button");
			els1.click();

			MobileElement el1 = driver.findElementByXPath("//android.view.View[@content-desc=\"Accedi / Registrati\"]/android.widget.TextView");
			el1.click();

			MobileElement els2 = driver.findElementByXPath("//android.widget.EditText[@resource-id='email']");
			els2.sendKeys(username());

			MobileElement els3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='password']");
			els3.sendKeys(password());

			MobileElement els4 = driver.findElementByXPath("//android.widget.Button[@text='ACCEDI']");
			els4.click();

			driver.navigate().back();

			String date = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.widget.TextView").getText();
			date = date.replaceAll("/","_");
			log.info(date);

			MobileElement el5 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.widget.Image");
			el5.click();

			MobileElement el50 = driver.findElementByXPath("//android.widget.Button[@text='Ok']");
			el50.click();

			MobileElement el6 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.widget.Image");
			el6.click();

			MobileElement el70 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.ImageView");
			el70.click();

			String lastThumbnailXpath = "//com.miles33.vnp2.TwoWayView//android.widget.RelativeLayout[last()]//android.widget.TextView";
			String secondLastThumbnailXpath = "//com.miles33.vnp2.TwoWayView//android.widget.RelativeLayout[last()-1]//android.widget.TextView";

			int imagesCount = getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el7 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.ImageView");
			el7.click();

			MobileElement el8 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
			el8.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
