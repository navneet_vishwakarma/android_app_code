package media_download;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Pocketmags --- program to download Pocketmags app publications
 * @author NA20251768
 *
 */
public class Pocketmags extends AppRelated {

	/*

Closer ----- FF ----- DONE
Chat ----- FV ----- DONE

	 */

	static int[] swipeCoordinates = {939, 1073, 88, 1031};
	static int[] thumbnailSwipeCoord = {907, 1550, 120, 1550};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Pocketmags.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			int flag = driver.findElementsById("com.triactivemedia.pocketmags:id/fragment_branded_welcome_button_next").size();

			if(flag >= 1) {
				MobileElement el1 = driver.findElementById("com.triactivemedia.pocketmags:id/fragment_branded_welcome_button_next");
				el1.click();

				MobileElement el2 = driver.findElementById("com.triactivemedia.pocketmags:id/fragment_branded_welcome_button_next");
				el2.click();

				MobileElement el3 = driver.findElementById("com.triactivemedia.pocketmags:id/fragment_branded_welcome_button_next");
				el3.click();

				MobileElement el4 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Options\"]/android.widget.ImageView");
				el4.click();

				MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Login / Register to Pocketmags']");
				els1.click();

				MobileElement el5 = driver.findElementById("com.triactivemedia.pocketmags:id/login_textinput_email");
				el5.sendKeys(username());

				MobileElement el6 = driver.findElementById("com.triactivemedia.pocketmags:id/login_radio_existing");
				el6.click();

				MobileElement el7 = driver.findElementById("com.triactivemedia.pocketmags:id/login_textinput_password1");
				el7.sendKeys(password());

				MobileElement el8 = driver.findElementById("com.triactivemedia.pocketmags:id/login_button_nextstep");
				el8.click();
			}

			MobileElement el9 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Browse\"]/android.widget.ImageView");
			el9.click();

			MobileElement el10 = driver.findElementById("com.triactivemedia.pocketmags:id/search_src_text");
			el10.sendKeys(keyWord);

			MobileElement el11 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.ImageView");
			el11.click();

			String date = driver.findElementById("com.triactivemedia.pocketmags:id/titlepage_textview_issue_name").getText();

			MobileElement el12 = driver.findElementById("com.triactivemedia.pocketmags:id/titlepage_imageview");
			el12.click();

			MobileElement el13 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.view.View");
			el13.click();

			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_framelayout_root'][last()]//android.widget.TextView[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_textview_pagenumber']";
			String secondLastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_framelayout_root'][last()-1]//android.widget.TextView[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_textview_pagenumber']";

			int imagesCount = getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el14 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.view.View");
			el14.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}

}
