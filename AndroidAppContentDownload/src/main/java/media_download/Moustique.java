package media_download;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Alza --- program to download Alza app publications
 * @author NA20251768
 *
 */
public class Moustique extends AppRelated {

	/*
Moustique ----- DONE

	 */

	static int[] swipeCoordinates = {1024, 1154, 71, 1151};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Moustique.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			MobileElement el1 = driver.findElementById("be.tecteo.moustique:id/coverDownloadButton");
			el1.click();
			MobileElement el2 = driver.findElementById("be.tecteo.moustique:id/txtEmail");
			el2.sendKeys(username());
			MobileElement el3 = driver.findElementById("be.tecteo.moustique:id/txtPassword");
			el3.sendKeys(password());
			MobileElement el4 = driver.findElementById("be.tecteo.moustique:id/btnConfirm");
			el4.click();
			
			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsByXPath("//android.view.View[@resource-id='app_navpos']/android.widget.TextView[2]").size();
				if(check == 0) {
					log.info("Loading");
				} else {
					break;
				}
			}

			String text = driver.findElementByXPath("//android.view.View[@resource-id='app_navpos']/android.widget.TextView[2]").getText();
			int imagesCount = Integer.parseInt(text);
			String date = getTodaysDate();
			
			log.info("The images count is " + imagesCount);

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
