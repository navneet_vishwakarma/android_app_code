package media_download;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * PressReader --- program to download PressReader app publications
 * @author NA20251768
 *
 */
public class PressReader2 extends AppRelated {

	/*

Daily Record ----- FF ----- DONE
Classic Car Weekly ----- FF
Bella ----- FF - Sorry, we couldn't find any publication for ""

The Washington Post ----- FF ----- DONE

	 */

	static int[] swipeCoordinates = {967, 978, 74, 978};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(PressReader2.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement els4 = driver.findElementByXPath("//android.widget.TextView[@text='Sign in']");
			els4.click();

			MobileElement el1 = driver.findElementByAccessibilityId("email");
			el1.click();

			MobileElement el2 = driver.findElementByAccessibilityId("authorization_user_name");
			el2.sendKeys(username());

			MobileElement el3 = driver.findElementByAccessibilityId("authorization_user_password");
			el3.sendKeys(password());

			MobileElement el4 = driver.findElementByAccessibilityId("authorization_signin");
			el4.click();

			for(int i = 0; i < 5; i++) {

				int check = driver.findElementsByXPath("//android.widget.TextView[@text='PressReader']").size();

				if(check >= 1) {
					log.info("Press reader found");
					break;
				}

				(new AndroidTouchAction(driver)).press(PointOption.point(536, 1535))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(536, 579))
				.release().perform();

				log.info("Scrolled Down");
			}

			MobileElement el40 = driver.findElementByXPath("//android.widget.TextView[@text='PressReader']");
			el40.click();

			Thread.sleep(5000);

			for(int i = 0; i < 10; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@resource-id='com.newspaperdirect.pressreader.android:id/loading_status_view_text']").size();

				if(check==0) {
					MobileElement el90 = driver.findElementById("com.newspaperdirect.pressreader.android:id/search_view");
					el90.click();
					log.info("Search Opened");
					break;
				} else {
					Thread.sleep(5000);
					log.info("Loaded again...");
				}
			}

			MobileElement el5 = driver.findElementById("com.newspaperdirect.pressreader.android:id/search_view");
			el5.sendKeys(keyWord);

			MobileElement el18 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView\n[@resource-id='com.newspaperdirect.pressreader.android:id/publications_detailed_items_view']/android.widget.FrameLayout[1]//android.widget.ImageView[@resource-id='com.newspaperdirect.pressreader.android:id/thumbnail']");
			el18.click();

			MobileElement el6 = driver.findElementByAccessibilityId("order_open");
			el6.click();


			MobileElement el7 = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='com.newspaperdirect.pressreader.android:id/single_page_view']");

			el7.click();

			el7.click();

			String date = driver.findElementById("com.newspaperdirect.pressreader.android:id/title_current_position").getText();
			log.info(date);

			MobileElement el8 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ViewSwitcher/android.widget.RelativeLayout/android.view.View");
			el8.click();

			int imagesCount = Integer.parseInt(date.substring(date.lastIndexOf(" ") + 1));

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}

}
