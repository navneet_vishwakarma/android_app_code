package media_download;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Aamulehti --- program to download Aamulehti app publications
 * @author NA20251768
 *
 */
public class Aamulehti extends AppRelated {

	/*
Aamulehti ----- DONE

	 */

	static int[] swipeCoordinates = {985, 1182, 127, 1175};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Aamulehti.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			MobileElement els1 = driver.findElementByXPath("//android.widget.Button[@text='OK']");
			els1.click();
			MobileElement els2 = driver.findElementByXPath("//android.widget.TextView[@text='Vahvista']");
			els2.click();
			MobileElement el1 = driver.findElementById("com.almamedia.kioski.aamulehti:id/ok");
			el1.click();
			MobileElement el2 = driver.findElementById("com.android.permissioncontroller:id/permission_allow_foreground_only_button");
			el2.click();
			MobileElement el3 = driver.findElementById("com.almamedia.kioski.aamulehti:id/login");
			el3.click();
			MobileElement el4 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[4]/android.widget.EditText[1]");
			el4.sendKeys(username());
			MobileElement el5 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[4]/android.widget.EditText[2]");
			el5.sendKeys(password());
			MobileElement els3 = driver.findElementByXPath("//android.widget.Button[@text='Kirjaudu']");
			els3.click();
			MobileElement els4 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'OK')]");
			els4.click();
			MobileElement els5 = driver.findElementByXPath("//android.widget.TextView[@text='Lehdet']");
			els5.click();

			MobileElement els13 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.almamedia.kioski.aamulehti:id/swipeRefresh']//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.almamedia.kioski.aamulehti:id/dateView']");
			String date = els13.getText().replaceAll(".", "/");
			System.out.println(date);
			els13.click();
			
			MobileElement els14 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.almamedia.kioski.aamulehti:id/swipeRefresh']//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.almamedia.kioski.aamulehti:id/image']");
			
			//els14.click();
			
			for(int i = 0; i < 4; i ++) {
				int check = driver.findElementsByAccessibilityId("Grid TOC").size();
				if(check == 0) {
					log.info("Loading");
					els14 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.almamedia.kioski.aamulehti:id/swipeRefresh']//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.almamedia.kioski.aamulehti:id/image']");
					els14.click();
				} else {
					break;
				}
			}
			
			int imagesCount = 200;
			
			//MobileElement els15 = driver.findElementByAccessibilityId("Grid TOC");
			//els15.click();
			
			log.info("The images count is " + imagesCount);

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
