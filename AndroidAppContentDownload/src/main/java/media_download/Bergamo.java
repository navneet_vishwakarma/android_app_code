package media_download;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Bergamo --- program to download Bergamo app publications
 * @author NA20251768
 *
 */
public class Bergamo extends AppRelated {

	static int[] swipeCoordinates = {985, 999, 92, 999};
	static int[] thumbnailSwipeCoord = {1024, 1719, 99, 1719};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Bergamo.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			AppRelated.waitForElementToLoad(driver.findElementsByXPath("//android.widget.TextView[@text='Mio Profilo']"));

			MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Mio Profilo']");
			els1.click();

			MobileElement els3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_username']");
			els3.sendKeys(username());

			MobileElement els4 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_password']");
			els4.sendKeys(password());

			MobileElement el2 = driver.findElementByAccessibilityId("Accedi");
			el2.click();

			Thread.sleep(7000);

			driver.navigate().back();

			String date = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Gallery/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.TextView").getText();
			date = date.replaceAll("/","_");
			log.info(date);

			if(checkOnlyDate(date)) {
				MobileElement el4 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Gallery/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.ImageView");
				el4.click();

				MobileElement el41 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
				el41.click();

				MobileElement els11 = driver.findElementByXPath("//android.widget.TextView[@text='Pagine']");
				els11.click();

				String lastThumbnailXpath = "//android.widget.AdapterView[@index=1]/child::android.widget.RelativeLayout[last()]//android.widget.TextView";
				String secondLastThumbnailXpath = "//android.widget.AdapterView[@index=1]/child::android.widget.RelativeLayout[last()-1]//android.widget.TextView";

				int imagesCount = AppRelated.getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

				log.info("The images count is " + imagesCount);

				MobileElement el22 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.AdapterView[1]/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageView");
				el22.click();

				tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
			}
			else {
				noMediaFound();
			}
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
