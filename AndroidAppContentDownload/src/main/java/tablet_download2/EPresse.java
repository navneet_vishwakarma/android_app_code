package tablet_download2;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * EPresse --- program to download EPresse app publications
 * @author NA20251768
 *
 */
public class EPresse extends AppRelated {

	/*	

	L'Humanit�  ----  DONE
	Tele Star  ----  DONE
	La Tribune de Mont�limar  ----  DONE
	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {572, 1599, 572, 471};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(EPresse.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			Thread.sleep(3000);
			try {
				MobileElement els11 = driver.findElementByXPath("//android.widget.Button[@text='ACCEPTER']");
				els11.click();
			} catch (Exception e) {
				MobileElement els12 = driver.findElementByXPath("//android.widget.Button[@text='Je d�couvre']");
				els12.click();
			}
			Thread.sleep(3000);
			MobileElement el1 = driver.findElementById("fr.epresse.android:id/profileButton");
			el1.click();
			try {
				MobileElement els12 = driver.findElementByXPath("//android.widget.Button[@text='Je d�couvre']");
				els12.click();
				Thread.sleep(3000);
				MobileElement el14 = driver.findElementById("fr.epresse.android:id/profileButton");
				el14.click();
			} catch (Exception e) {
			}
			Thread.sleep(3000);
			MobileElement el2 = driver.findElementById("fr.epresse.android:id/myAccountButton");
			el2.click();
			Thread.sleep(3000);
			MobileElement el3 = driver.findElementById("fr.epresse.android:id/email");
			el3.click();
			Thread.sleep(3000);
			MobileElement els2 = driver.findElementByXPath("//android.widget.EditText[@resource-id='emailConnectVerif']");
			els2.sendKeys(username());
			MobileElement els5 = driver.findElementByXPath("//android.view.View[@resource-id='emailConnectVerifForm']/android.widget.EditText[2]");
			els5.sendKeys(password());
			MobileElement els6 = driver.findElementByXPath("//android.widget.Button[@text='Se connecter']");
			els6.click();

			Thread.sleep(10000);

			MobileElement el5 = driver.findElementByAccessibilityId("Recherche");
			el5.click();
			Thread.sleep(7000);

			for(int i = 0; i < 4;i++) {
				int count = driver.findElementsById("fr.epresse.android:id/search_text").size();
				if(count == 0) {
					try {
						el5 = driver.findElementByAccessibilityId("Recherche");
						el5.click();
					} catch (Exception e) {
						log.info("Recherche not found");
					}
				} else {
					break;
				}
			}
			MobileElement el7 = driver.findElementById("fr.epresse.android:id/search_text");
			el7.sendKeys(keyWord);
			MobileElement els8 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]");
			els8.click();
			Thread.sleep(7000);
			MobileElement els9 = driver.findElementByXPath("//android.widget.TextView[@resource-id='fr.epresse.android:id/issueDateAndNumber']");
			String date = els9.getText();
			log.info(date);
			MobileElement els10 = driver.findElementByXPath("//android.widget.TextView[@text='T�l�charger']");
			els10.click();
			MobileElement els11 = driver.findElementByXPath("//android.widget.Button[@text='OK']");
			els11.click();
			for(int i = 0; i < 20; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[contains(@text,'Lire')]").size();
				if(check == 0) {
					log.info("Loaded again...");
					try {
						els10 = driver.findElementByXPath("//android.widget.TextView[@text='T�l�charger']");
						els10.click();
					} catch (Exception e) {

					}
				} else {
					log.info("Image opened");
					break;
				}
			}
			MobileElement els12 = driver.findElementByXPath("//android.widget.TextView[@text='Lire']");
			els12.click();
			try {
				MobileElement els13 = driver.findElementByXPath("//android.widget.Button\n[@text='NO']");
				els13.click();
			} catch (Exception e) {

			}
			Thread.sleep(3000);
			MobileElement els14 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='fr.epresse.android:id/pspdf__document_view']//android.widget.FrameLayout");
			els14.click();
			Thread.sleep(3000);
			MobileElement els15 = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='fr.epresse.android:id/splitview']");
			els15.click();

			Thread.sleep(7000);

			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[last()]//android.widget.TextView";

			int imagesCount = 0;

			for(int j = 0; j < 10; j++) {

				(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
				.release().perform();

				Thread.sleep(5000);

			}

			String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
			imagesCount = Integer.valueOf(text.substring(5));

			log.info("The images count is " + imagesCount);

			driver.navigate().back();
			Thread.sleep(3000);
			MobileElement els19 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='fr.epresse.android:id/pspdf__document_view']//android.widget.FrameLayout");
			els19.click();
			Thread.sleep(4000);
			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
