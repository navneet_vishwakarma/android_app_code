package tablet_download2;

import java.time.Duration;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * LaPrealpine --- program to download LaPrealpine app publications
 * @author NA20251768
 *
 */
public class LaPrealpine extends AppRelated {

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {1099, 1731, 195, 1731};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(LaPrealpine.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);


			for(int i = 0; i<5; i++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='Mio Profilo']").size();
				if(check!=0) {
					log.info("Loading");
				} else {
					log.info("Element found");
					Thread.sleep(3000);
					break;
				}
			}
			
			
			MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Mio Profilo']");
			els1.click();
			MobileElement els2 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_username']");
			els2.sendKeys(username());
			MobileElement els3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='input_password']");
			els3.sendKeys(password());
			MobileElement els4 = driver.findElementByXPath("//android.view.View[@text='ACCEDI' or @text='Accedi']");
			els4.click();

			for(int i = 0; i<5; i++) {
				int check = driver.findElementsByXPath("//android.view.View[@text='SCOLLEGA IL TUO DISPOSITIVO']").size();
				if(check!=0) {
					log.info("Loading");
				} else {
					log.info("Element found");
					Thread.sleep(3000);
					break;
				}
			}
			
			MobileElement els5 = driver.findElementByXPath("//android.widget.TextView[@text='Home']");
			els5.click();
			
			String download = driver.findElementByXPath("//android.widget.Gallery/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView").getText();
			if(download.equals("ANTEPRIMA")) {
				(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
				.release().perform();
				Thread.sleep(3000);
			}
			
			String date = driver.findElementByXPath("//android.widget.Gallery/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.TextView").getText();
			log.info(date);

			MobileElement els6 = driver.findElementByXPath("//android.widget.Gallery/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView");
			els6.click();
			
			Thread.sleep(5000);
			
			MobileElement els7 = driver.findElementByXPath("//android.widget.ImageView");
			els7.click();
			
			MobileElement els8 = driver.findElementByXPath("//android.widget.TextView[@text='Pagine']");
			els8.click();
			
			String lastThumbnailXpath = "//android.widget.RelativeLayout/android.widget.AdapterView[2]/android.widget.RelativeLayout[last()]//android.widget.TextView";
			String secondLastThumbnailXpath = "//android.widget.RelativeLayout/android.widget.AdapterView[2]/android.widget.RelativeLayout[last()-1]//android.widget.TextView";
			
			boolean secondThumbCheck = false;
			for(int i = 0; i < 4; i ++) {
				int check1 = driver.findElementsByXPath(lastThumbnailXpath).size();
				int check2 = driver.findElementsByXPath(secondLastThumbnailXpath).size();
				if(check1 == 0) {
					if(check2 == 0) {
						log.info("Loaded again...");
					} else {
						log.info("Image found");
						lastThumbnailXpath = secondLastThumbnailXpath;
						secondThumbCheck = true;
						break;
					}
				} else {
					log.info("Image found");
					break;
				}
			}
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getText().replaceAll(" ", "");

			log.info(text1 + " = text1");
			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 6; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);
					
					String text = null;
					try {
						text = driver.findElementByXPath(lastThumbnailXpath).getText().replaceAll(" ", "");
						log.info(text1 + " = text2");
					} catch (NoSuchElementException e) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
					}
					try {
						imagesCount = Integer.parseInt(text);
					} catch (NumberFormatException n) {
						log.info("NumberFormatException in text " + text);
						
						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
						
						Thread.sleep(7000);
						
						try {
							text = driver.findElementByXPath(lastThumbnailXpath).getText().replaceAll(" ", "");
							log.info(text + " = text");
						} catch (NoSuchElementException n1) {
							text = driver.findElementByXPath(secondLastThumbnailXpath).getText().replaceAll(" ", "");
							log.info(text + " = text");
						}
						
						imagesCount = Integer.parseInt(text);
					}
					if(imagesCount == count && (secondThumbCheck == true)) {
						imagesCount = imagesCount + 1;
						break;
					} else if(imagesCount == count && (secondThumbCheck == false)) {
						break;
					}
					else count = imagesCount;
				}
			}
			
			log.info("Images count " + imagesCount);
			Thread.sleep(3000);
			
			MobileElement els10 = driver.findElementByXPath("//android.widget.RelativeLayout/android.widget.AdapterView[1]//android.widget.ImageView");
			els10.click();
			
			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}
}
