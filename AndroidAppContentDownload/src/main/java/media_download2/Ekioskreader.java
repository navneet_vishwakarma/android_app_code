package media_download2;

import java.time.Duration;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * LesEchos --- program to download LesEchos app publications
 * @author NA20251768
 *
 */
public class Ekioskreader extends AppRelated {

	/*	

	Les Echos ----- DONE
	Les Echos Weekend ----- DONE
	
	 */

	static int[] swipeCoordinates = {904, 1066, 166, 1073};
	static int[] thumbnailSwipeCoord = {925, 1871, 191, 1871};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Ekioskreader.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/user");
			el1.sendKeys(username());
			MobileElement el2 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/password");
			el2.sendKeys(password());
			MobileElement el3 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/login");
			el3.click();
			MobileElement el4 = driver.findElementById("com.android.permissioncontroller:id/permission_allow_button");
			el4.click();
			MobileElement el5 = driver.findElementByAccessibilityId("Search");
			el5.click();
			MobileElement el6 = driver.findElementById("android:id/search_src_text");
			el6.sendKeys(app);
			
			Thread.sleep(5000);

			KeyInput keyboard = new KeyInput("keyboard");
			Sequence sendKeys = new Sequence(keyboard, 0);
			sendKeys.addAction(keyboard.createKeyDown(Keys.ENTER.getCodePoint()));

			driver.perform(Arrays.asList(sendKeys));

			Thread.sleep(5000);
			
			
			MobileElement el7 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/cover");
			el7.click();

			MobileElement els3 = driver.findElementByXPath("//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]");
			els3.click();
			MobileElement el8 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/read");
			el8.click();
			MobileElement el9 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/reader");
			el9.click();
			
			String lastThumbnailXpath = "//android.support.v7.widget.RecyclerView/android.widget.FrameLayout[last()]//android.widget.TextView";
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
					imagesCount = Integer.parseInt(text);

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}
			
			log.info("The images count is " + imagesCount);
			
			MobileElement els6 = driver.findElementById("com.ekioskreader.android.pdfviewer:id/reader");
			els6.click();

			tearDown(imagesCount, "07102021", keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
