package utility;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

public class Demo {
	
	public static void main(String[] args) {
		File rootOutput = new File("Output_Tablet"+ 
				File.separator +"Turkey"+ 
						File.separator +"Daily"+ 
								File.separator +"Dergilik"+ 
										File.separator +"2022"+ 
												File.separator +"07_2022");
		String outputFile = "11_07_2022_Dergilik_Fanatik.pdf";
		String copyfilenamepath = rootOutput + "\\" + outputFile;
		File file = new File(copyfilenamepath);
		System.out.println("filepathhorreum--" + copyfilenamepath);
		String pubDate = copyfilenamepath.substring(copyfilenamepath.lastIndexOf("\\") + 1)
				.substring(0, 10).replaceAll("_", "-");
		System.out.println("datehorreum--" + copyfilenamepath.substring(copyfilenamepath.lastIndexOf("\\") + 1).substring(0, 10));
		System.out.println(pubDate);
		
		String datehorreum = copyfilenamepath.substring(copyfilenamepath.lastIndexOf("\\") + 1).substring(0, 10).replaceAll("_", "-");
		System.out.println("datehorreum--" + datehorreum);
		String[] arr = datehorreum.split("-");
		String publicationDate = "";
		for(int i = arr.length - 1; i >= 0; i--) {
			publicationDate = publicationDate + arr[i] + "-";
		}
		publicationDate = publicationDate.substring(0, publicationDate.length() - 1);
		System.out.println(publicationDate);
	}
}
