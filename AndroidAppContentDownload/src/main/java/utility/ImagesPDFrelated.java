package utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

/**
 * ImagesPDFrelated --- program to capture images and generate PDF
 * @author NA20251768
 *
 */
public class ImagesPDFrelated extends AppRelated {
	
	static ReadWrite rw;
	
	static File rootOutput;
	
	static String outputFile;

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(ImagesPDFrelated.class.getName());

	public void generatePDF(String date, String keyWord, String app) throws DocumentException, IOException {
		
		rw = new ReadWrite();
		
		File root = new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord);

		rootOutput = new File(rw.readProperties().getProperty("pdfOutputPath") + 
				File.separator + rw.excelReader(app, keyWord, "country") + 
				File.separator + rw.excelReader(app, keyWord, "frequency") + 
				File.separator + app +
				File.separator + year +
				File.separator + month + "_" + year);
		rootOutput.mkdirs();

		int fileCount = root.list().length;
		//String outputFile = app + "_" + keyWord + "_"+ date.replace(" ", "_") + ".pdf";
		outputFile = todayDate + "_" + month + "_" + year + "_" + app + "_" + keyWord + ".pdf";
		List<String> files = new ArrayList<>();
		for(int i = 1; i <= fileCount; i++) {
			files.add("P" + i + ".png");
			log.info("P" + i + ".png");
		}

		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(new File(rootOutput, outputFile.replaceAll("[|]",""))));
		document.open();
		for (String f : files) {
			document.newPage();
			Image image = Image.getInstance(new File(root, f).getAbsolutePath());
			image.setAbsolutePosition(0, 0);
			image.setBorderWidth(0);
			image.scaleAbsolute(PageSize.A4);
			document.add(image);
		}
		document.close();
		log.info("PDF output folder " + rootOutput);
		log.info("PDF Created " + outputFile);
		downloadedFileName = outputFile;
		
        Date dt = new Date();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        downloadedDate = formatter.format(dt);
		log.info("Downloaded Date is " + downloadedDate +  " UTC");
		
		rw.excelWriter(app, keyWord, "downloadedFile", outputFile, downloadedDate);
	}

	public void deleteFiles(String app, String keyWord) throws IOException {
		rw = new ReadWrite();
		Path path = Paths.get(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord);
		File directory = new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord);
		boolean tryMe = directory.exists();

		if(tryMe) {

			// delete each file from the directory
			
			try (Stream<Path> walk = Files.walk(path)) {
			    walk.sorted(Comparator.reverseOrder())
			        .map(Path::toFile)
			        .forEach(File::delete);
			    log.info("Directory Deleted");
			}
			
		}
	}

	public void captureImages(AppiumDriver<MobileElement> driver, String app, String keyWord, int imagesCount, int[] swipeCoordinates) throws IOException, InterruptedException {
		rw = new ReadWrite();
		TakesScreenshot sc = driver;
		File src = sc.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P1.png"));
		log.info(1 + " screenshot taken");

		for(int i = 2; i < imagesCount + 1; i++) {

			(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
			.release().perform();
			
			Thread.sleep(1000);

			sc = driver;
			src = sc.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P" + i +".png"));
			log.info(i + " screenshot taken");

		}
	}
	
	public void captureImagesWait(AppiumDriver<MobileElement> driver, String app, String keyWord, int imagesCount, int[] swipeCoordinates) throws IOException, InterruptedException {
		rw = new ReadWrite();
		TakesScreenshot sc = driver;
		File src = sc.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P1.png"));
		log.info(1 + " screenshot taken");

		for(int i = 2; i < imagesCount + 1; i++) {

			(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
			.release().perform();
			
			Thread.sleep(6000);
			sc = driver;
			src = sc.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P" + i +".png"));
			log.info(i + " screenshot taken");

		}
	}
	
	/* To upload a pdf file to horreum */
	public static void horreumUploadFile() throws Exception {
		rw = new ReadWrite();
		String horreumLoginURL = rw.readProperties().getProperty("horreumLoginURL");
		String horreumUsername = rw.readProperties().getProperty("horreumUsername");
		String horreumPassword = rw.readProperties().getProperty("horreumPassword");
		String horreumUploadURL = rw.readProperties().getProperty("horreumUploadURL");
		log.info("Inside horreum method");
		long start = System.currentTimeMillis();
		Thread.sleep(3000);
		HttpClient httpClient = HttpClients.createDefault();
		RequestBuilder reqbuilder = RequestBuilder.post(horreumLoginURL);
		reqbuilder.addParameter("login_user", horreumUsername);
		reqbuilder.addParameter("login_password", horreumPassword);
		log.info(horreumLoginURL + "," + horreumUsername + "," + horreumPassword);
		Thread.sleep(3000);
		HttpUriRequest multipartRequest = reqbuilder.build();
		HttpResponse httpresponse = null;
		try {
			httpresponse = httpClient.execute(multipartRequest);
		} catch (Exception e) {
			log.info("exception in horreum login" + e);
			Thread.sleep(5000);
			httpClient = HttpClients.createDefault();
			reqbuilder = RequestBuilder.post(horreumLoginURL);
			reqbuilder.addParameter("login_user", horreumUsername);
			reqbuilder.addParameter("login_password", horreumPassword);
			log.info(horreumLoginURL + "," + horreumUsername + "," + horreumPassword);
			Thread.sleep(3000);
			multipartRequest = reqbuilder.build();
			httpresponse = httpClient.execute(multipartRequest);
		}
		log.info(EntityUtils.toString(httpresponse.getEntity()));
		log.info(httpresponse.getStatusLine());
		log.info("Logged in successfully");
		Thread.sleep(3000);
		String copyfilenamepath = rootOutput + "\\" + outputFile;
		File file = new File(copyfilenamepath);
		log.info("filepathhorreum--" + copyfilenamepath);
		Thread.sleep(3000);
		FileBody filebody = new FileBody(file, ContentType.MULTIPART_FORM_DATA);
		//StringBody pubDate = new StringBody(copyfilenamepath.substring(copyfilenamepath.lastIndexOf("\\") + 1)
		//		.substring(0, 10).replaceAll("_", "-"), ContentType.MULTIPART_FORM_DATA);
		
		String datehorreum = copyfilenamepath.substring(copyfilenamepath.lastIndexOf("\\") + 1).substring(0, 10).replaceAll("_", "-");
		System.out.println("datehorreum--" + datehorreum);
		String[] arr = datehorreum.split("-");
		String publicationDate = "";
		for(int i = arr.length - 1; i >= 0; i--) {
			publicationDate = publicationDate + arr[i] + "-";
		}
		publicationDate = publicationDate.substring(0, publicationDate.length() - 1);
		System.out.println(publicationDate);
		StringBody pubDate = new StringBody(publicationDate, ContentType.MULTIPART_FORM_DATA);
		
		StringBody pcdid = new StringBody(mediaID, ContentType.MULTIPART_FORM_DATA);
		MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
		entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		entityBuilder.addPart("file", filebody);
		entityBuilder.addPart("publicationDate", pubDate);
		entityBuilder.addPart("pcdid", pcdid);
		HttpEntity multiPartHttpEntity = entityBuilder.build();
		reqbuilder = RequestBuilder.post(horreumUploadURL);
		reqbuilder.setEntity(multiPartHttpEntity);
		reqbuilder.addHeader("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36");
		reqbuilder.addHeader("accept", "*/*");
		reqbuilder.addHeader("accept-encoding", "gzip, deflate, br");
		reqbuilder.addHeader("accept-language", "en-US,en;q=0.9");
		reqbuilder.addHeader("origin", "https://horreum.app.cision.com");
		reqbuilder.addHeader("referer", "https://horreum.app.cision.com");
		reqbuilder.addHeader("sec-fetch-dest", "empty");
		reqbuilder.addHeader("sec-fetch-mode", "cors");
		reqbuilder.addHeader("sec-fetch-site", "same-origin");

		multipartRequest = reqbuilder.build();
		httpresponse = httpClient.execute(multipartRequest);
		log.info(EntityUtils.toString(httpresponse.getEntity()));
		log.info(httpresponse.getStatusLine());
		if (String.valueOf(httpresponse.getStatusLine()).contains("200")) {
			log.info("Uploaded successfully");
			status = "HORREUM UPLOAD - PASSED";
			isUploadedInHorreum = "UPLOADED";
		} else {
			log.info("Upload part failed");
			Assert.assertEquals(true, false);
		}

		long end = System.currentTimeMillis();
		System.out.println("Time taken for horreumUploadFile() is  " + (end - start) + "ms");
		Thread.sleep(2000);
	}
	
	/* To upload the downloaded file in Media Archive network drive */
	public static void networkDriveUploadFile() throws Exception {
		rw = new ReadWrite();
		String networkDrive = rw.readProperties().getProperty("networkDrive");
		String frequency = rw.excelReader(app, keyWord, "frequency");
		String finalFilePath = networkDrive + 
				   File.separator + "Automated Files" +
				   File.separator + rw.excelReader(app, keyWord, "country") + 
				   File.separator + frequency + 
				   File.separator + app +
				   File.separator + year +
				   File.separator;
		if (frequency.equalsIgnoreCase("Weekly")) {
			finalFilePath = finalFilePath + month + 
					File.separator + outputFile;
		} else if(frequency.equalsIgnoreCase("Daily")){
			finalFilePath = finalFilePath + month + 
					File.separator + outputFile;
		} else {
			finalFilePath = finalFilePath + month + 
					File.separator + outputFile;
		}
		
		String copyfilenamepath = rootOutput + "\\" + outputFile;
		log.info("localPath--" + copyfilenamepath);
		log.info("networkPath--" + finalFilePath);
		File source = new File(copyfilenamepath);
		File destination = new File(finalFilePath);
		moveFile(source, destination);
	}
}
