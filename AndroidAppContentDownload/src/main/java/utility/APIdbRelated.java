package utility;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class APIdbRelated extends AppRelated {
	
	public static void dbInsertion() throws IOException {

		long end = System.currentTimeMillis();
		int seconds = (int) ((end - (start == 0 ? end : start)) / 1000);
        System.out.println("Total time taken for script execution = " + seconds + " seconds");
        
		ReadWrite rw = new ReadWrite();
		String apiURL = rw.readProperties().getProperty("apiURL");
		
		String overallStatus = status.contains("PASSED") ? "SUCCESS" : "FAILURE";
		
		System.out.println("Started DB insertion using API - "+ apiURL);
		String requestBody = "{\n" +
				" \"mediaId\": \""+ mediaID +"\", \n" +
				" \"deviceId\": \""+ deviceID +"\", \n"  +
				" \"sharedDriveFileLocation\": \""+ sharedDriveFileLocation +"\", \n"  +
				" \"horreumUploadStatus\": \""+ isUploadedInHorreum +"\", \n"  +
				((downloadedDate == "") ? "" : " \"downloadedDate\": \""+ downloadedDate +"\", \n"  ) +
				" \"downloadedFileName\": \""+ downloadedFileName +"\", \n" +
				" \"status\": \""+ overallStatus +"\", \n" +
				" \"durationInSec\": \""+ seconds +"\", \n" +
				" \"sDUploadStatus\": \""+ isUploadedInSharedDrive +"\" \n}";
				RestAssured.baseURI = apiURL;
				Response response = given()
						.header("Content-type", "application/json")
						.and()
						.body(requestBody)
						.when()
						.post("/transactions")
						.then()
						.extract().response();
				System.out.println("API response for DB insertion: " + response.statusLine());
	}

}
