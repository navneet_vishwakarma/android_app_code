package utility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TEST {

	public static void main(String[] args) throws IOException {

		Date dt = new Date();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateCreated = formatter.format(dt);
		System.out.println("Script running at - " + dateCreated + " UTC");
	}
}
