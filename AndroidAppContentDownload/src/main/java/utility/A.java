package utility;

public class A {


public void connectToRemoteServer()
{
  try
  {
    String USER_NAME = "domain\\username";
    String PASSWORD = "password";
    /** ATTEMPT 1 - Call command from JAVA using string - OK in LOCAL, FAILED in UAT */
    String commandInside = "net use " + "\\\\ipaddress\\folder /user:" + USER_NAME + " " + PASSWORD;
    Process p = Runtime.getRuntime().exec(commandInside);
    System.out.println("Executing command [" + commandInside + "]");
    p.waitFor();

  }
  catch (Exception e)
  {
    e.printStackTrace();
  }
}


}
