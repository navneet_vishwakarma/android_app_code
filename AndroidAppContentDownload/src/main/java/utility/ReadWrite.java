package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * ReadWrite --- program to read, write property or any other file
 * @author NA20251768
 *
 */
public class ReadWrite {
	
	static FileReader reader;  
    
	public static Properties p;
	
	static Logger log = Logger.getLogger(ReadWrite.class.getName());
		
	static ReadWrite rw;
	
	Map<String, Integer> heading;
	
	public Properties readProperties() throws IOException {
		reader=new FileReader("Settings/config.properties");  
	      
	    p=new Properties();  
	    p.load(reader);
	      
	    return p;
	}
	
	public String excelReader(String app, String keyword, String head) throws IOException {
		rw = new ReadWrite();
		
		PropertyConfigurator.configure(rw.readProperties().getProperty("logPropertiesPath"));
		
		String excelFilePath = rw.readProperties().getProperty("excelPath");
		heading = headingMap();
		
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        
        String output = "";
         
        try (Workbook workbook = new XSSFWorkbook(inputStream)) {
			Sheet firstSheet = workbook.getSheetAt(0);
			
			/*
			Iterator<Row> iterator = firstSheet.iterator();
			 
			while (iterator.hasNext()) {
			    Row nextRow = iterator.next();
			    Iterator<Cell> cellIterator = nextRow.cellIterator();
			    System.out.println("Changing row");
			    while (cellIterator.hasNext()) {
			        Cell cell = cellIterator.next();
			         System.out.println(cell.getCellType());
			         System.out.println(cell.getStringCellValue());
			        System.out.print(" - ");
			    }
			    System.out.println();
			}
			
			*/
			System.out.println("==============");
			
			Iterator<Row> iterator = firstSheet.iterator();
			int maxRows = 0;
			while (iterator.hasNext()) {
				maxRows++;
				iterator.next();
			}
			
			int columnIndex = heading.get("keyWord");
			int rowIndex = 1;
			for(rowIndex = 1; rowIndex <= maxRows; rowIndex++) {
			    Row row = CellUtil.getRow(rowIndex, firstSheet);
			    Cell cell = CellUtil.getCell(row, columnIndex);
			    if(cell.getStringCellValue().equals(keyword)) {
			    	row = firstSheet.getRow(rowIndex);
			    	cell = row.getCell(heading.get("apps"));
			    	if(cell.getStringCellValue().equals(app)) {
			    		log.info(app + " & " +  keyword + " is found");
			    		break;
			    	}
			    }
			}
			Row row = firstSheet.getRow(rowIndex);
			Cell cell = row.getCell(heading.get(head));
			log.info(cell.getStringCellValue());
			
			output =  cell.getStringCellValue();
			
			workbook.close();
		}
        inputStream.close();
        return output;
	}
	
	public void excelWriter(String app, String keyword, String head, String outputFile, String dwnDate) throws IOException {
		rw = new ReadWrite();
		
		PropertyConfigurator.configure(rw.readProperties().getProperty("logPropertiesPath"));
		
		String excelFilePath = rw.readProperties().getProperty("excelPath");
		heading = headingMap();
		
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
         
        try (Workbook workbook = new XSSFWorkbook(inputStream)) {
			Sheet firstSheet = workbook.getSheetAt(0);
			System.out.println("==============");
			
			Iterator<Row> iterator = firstSheet.iterator();
			int maxRows = 0;
			while (iterator.hasNext()) {
				maxRows++;
				iterator.next();
			}
			
			int columnIndex = heading.get("keyWord");
			int rowIndex = 1;
			for(rowIndex = 1; rowIndex <= maxRows; rowIndex++) {
			    Row row = CellUtil.getRow(rowIndex, firstSheet);
			    Cell cell = CellUtil.getCell(row, columnIndex);
			    if(cell.getStringCellValue().equals(keyword)) {
			    	row = firstSheet.getRow(rowIndex);
			    	cell = row.getCell(heading.get("apps"));
			    	if(cell.getStringCellValue().equals(app)) {
			    		log.info(app + " & " +  keyword + " is found");
			    		break;
			    	}
			    }
			}
			Row row = firstSheet.getRow(rowIndex);
			Cell cell = row.getCell(heading.get(head));
			cell.setCellValue(outputFile);
			
			Cell dateCell = row.getCell(heading.get("downloadedDate"));
			dateCell.setCellValue(dwnDate);

			try (FileOutputStream outputStream = new FileOutputStream(new File(excelFilePath))) {
	            workbook.write(outputStream);
	            workbook.close();
	            outputStream.close();
	        }
			
		}
        inputStream.close();
	}
	
	private Map<String, Integer> headingMap() {
		Map<String, Integer> heading = new HashMap<>();
		heading.put("apps", 1);
		heading.put("keyWord", 2);
		heading.put("appPackage", 3);
		heading.put("appActivity", 4);
		heading.put("username", 5);
		heading.put("password", 6);
		heading.put("udid", 7);
		heading.put("country", 8);
		heading.put("frequency", 9);
		heading.put("testData1", 10);
		heading.put("testData2", 11);
		heading.put("testData3", 12);
		heading.put("testData4", 13);
		heading.put("horreumUpload", 14);
		heading.put("sharedDriveUpload", 15);
		heading.put("mediaID", 16);
		heading.put("downloadedDate", 17);
		heading.put("downloadedFile", 18);
		
		return heading;
	}

}
