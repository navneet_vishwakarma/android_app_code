package utility;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import com.itextpdf.text.DocumentException;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

/**
 * AppRelated --- program to initialize app and download PDF 
 * @author NA20251768
 *
 */
public class AppRelated {

	protected AppRelated() {
		//not called
	}

	protected static AppiumDriver<MobileElement> driver;
	protected static DesiredCapabilities caps;

	protected static ImagesPDFrelated img;
	protected static ReadWrite rw;

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(AppRelated.class.getName());
		
	protected static String app = "";
	
	protected static String keyWord = "";
	
	protected static String year = "";
	
	protected static String month = "";
	
	protected static String todayDate = "";
	protected static String isUploadedInSharedDrive = "NOT APPLICABLE";
	
	protected static String status = "FAILED";
	//FAILED, LOCAL DOWNLOAD - PASSED, HORREUM UPLOAD - FAILED, HORREUM UPLOAD - PASSED SHARED DRIVE UPLOAD - FAILED, SHARED DRIVE UPLOAD - PASSED.
	protected static String sharedDriveFileLocation = "";
	protected static String isUploadedInHorreum = "NOT APPLICABLE";
	//UPLOADED, FAILED, NOT APPLICABLE
	protected static String mediaID = "";
	protected static String deviceID = "";
	protected static String downloadedDate = "";
	protected static String dateCreated = "";
	protected static String downloadedFileName = "";
	protected static long start = 0;
	
	public static String username() throws IOException {
		rw = new ReadWrite();
		String username = rw.excelReader(app, keyWord, "username");
		log.info(username);
		return username;
	}

	public static String password() throws IOException {
		rw = new ReadWrite();
		String password = rw.excelReader(app, keyWord, "password");
		log.info(password);
		return password;
	}
	
	public static String horreumFlag() throws IOException {
		rw = new ReadWrite();
		String horreumFlag = rw.excelReader(app, keyWord, "horreumUpload");
		log.info("Upload to horreum" + horreumFlag);
		return horreumFlag;
	}
	
	public static String sharedDriveFlag() throws IOException {
		rw = new ReadWrite();
		String sharedDriveFlag = rw.excelReader(app, keyWord, "sharedDriveUpload");
		log.info("Upload to shared drive" + sharedDriveFlag);
		return sharedDriveFlag;
	}
	
	public static String publicationID() throws IOException {
		rw = new ReadWrite();
		String publicationID = rw.excelReader(app, keyWord, "mediaID");
		log.info(publicationID);
		return publicationID;
	}

	public static AppiumDriver<MobileElement> setUp(String[] args) throws IOException {
		
		try {
			app = args[0];
			keyWord = args[1];
			
			//app = "Alza";
			//keyWord = "Pravo";

		} catch (ArrayIndexOutOfBoundsException e){
	        System.out.println("No keyword Parameter passed");
	    }
		
		start = System.currentTimeMillis();
		
		mediaID = publicationID();
		rw = new ReadWrite();
		deviceID = rw.excelReader(app, keyWord, "udid");	
		downloadedDate = "";
		
		Date dt = new Date();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC")); // For IST - "Asia/Kolkata"
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateCreated = formatter.format(dt);
		log.info("Automation Script started at - " + dateCreated + " UTC");
		
		PropertyConfigurator.configure(rw.readProperties().getProperty("logPropertiesPath"));
		
		caps = new DesiredCapabilities();
		caps.setCapability("platformName", "Android");
		caps.setCapability("udid", deviceID);
		caps.setCapability("automationName", "UiAutomator2");
		caps.setCapability("appPackage", rw.excelReader(app, keyWord, "appPackage"));
		caps.setCapability("appActivity", rw.excelReader(app, keyWord, "appActivity"));
		caps.setCapability("ensureWebviewsHavePages", true);
		URL url = new URL(rw.readProperties().getProperty("serverUrl"));

		driver = new AppiumDriver<>(url, caps);
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(rw.readProperties().getProperty("implicitWait")), TimeUnit.SECONDS);
		log.info("App Launched");
		
		getDate("dd/MM/yyyy", "IST");
		
		return driver;
	}
	
	public static void noMediaFound() {
		log.info("Todays publication is not available/published");
		log.info("Error! Fail Job!");
		Assert.fail();
	}
	
	public static void handleException(Exception e) throws Exception {
		log.error("Exception--" + e);
        log.error("Error! Fail Job!");
        throw e;
	}

	public static void tearDown(int imagesCount, String date, String keyWord, String app, int[] swipeCoordinates) throws Exception {

		img = new ImagesPDFrelated();

		img.deleteFiles(app, keyWord);

		Thread.sleep(10000);

		img.captureImages(driver, app, keyWord, imagesCount, swipeCoordinates);

		driver.quit();
		
		img.generatePDF(date, keyWord, app);
		
		status = "LOCAL DOWNLOAD - PASSED";
		
		String uploadToHorreumcheck = horreumFlag();
		if (uploadToHorreumcheck.equalsIgnoreCase("Yes")) {
			isUploadedInHorreum = "FAILED";
			status = "HORREUM UPLOAD - FAILED";
			ImagesPDFrelated.horreumUploadFile();
			
		} else {
			log.info("horreum upload flag is off");
		}
		
		String uploadToSharedDrivecheck = sharedDriveFlag();
		if (uploadToSharedDrivecheck.equalsIgnoreCase("Yes")) {
			isUploadedInSharedDrive = "FAILED"; 
			status = "SHARED DRIVE UPLOAD - FAILED";
			ImagesPDFrelated.networkDriveUploadFile();
			
		} else {
			log.info("Shared drive upload flag is off");
		}
	}
	
	
	
	public static void tearDownWait(int imagesCount, String date, String keyWord, String app, int[] swipeCoordinates) throws IOException, DocumentException, InterruptedException {

		img = new ImagesPDFrelated();

		img.deleteFiles(app, keyWord);

		Thread.sleep(10000);

		img.captureImagesWait(driver, app, keyWord, imagesCount, swipeCoordinates);

		img.generatePDF(date, keyWord, app);
	}
	
	public static void waitForElementToLoad(List<MobileElement> element) throws InterruptedException {
		
		for(int i = 0; i < 10; i++) {
			int check = element.size();
			if(check == 0) {
				log.info("Loaded Again...");
				Thread.sleep(10000);
			} else {
				log.info("Element opened");
				break;
			}
		}
		
	}
	
	public static String getTodaysDate() {
		Date date1 = new Date(); 
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		formatter.setTimeZone(TimeZone.getTimeZone("IST"));
		String todayDate = formatter.format(date1);
		
		System.out.println("Todays date = " + todayDate);
		return todayDate;
	}
	
	public static boolean checkDateGreaterThanCurrent(String date) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date date1 = new Date(); 
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		formatter.setTimeZone(TimeZone.getTimeZone("IST"));
		String todayDate = formatter.format(date1);
		
		System.out.println("Todays date = " + todayDate);
		
		System.out.println("Date Compare = " + sdf.parse(todayDate).before(sdf.parse(date)));
		
		return sdf.parse(todayDate).before(sdf.parse(date));
	}
	
	public static boolean checkFullDate(String date) {
		String[] todaysDate = getDate("dd/MM/yyyy", "IST");
		return date.contains(todaysDate[0]);
	}
	
	public static boolean checkOnlyDate(String date) {
		String[] todaysDate = getDate("dd/MM/yyyy", "IST");
		return date.contains(todaysDate[1]);
	}
	
	public static boolean checkOnlyMonth(String date) {
		String[] todaysDate = getDate("dd/MM/yyyy", "IST");
		return date.contains(todaysDate[2]);
	}
	
	public static boolean checkOnlyYear(String date) {
		String[] todaysDate = getDate("dd/MM/yyyy", "IST");
		return date.contains(todaysDate[3]);
	}
	
	public static String[] getDate(String format, String timezone) {
		// TODO Auto-generated method stub
		Date date = new Date();  
		String strDate[] = new String[4];
		SimpleDateFormat formatter = new SimpleDateFormat(format);  
		formatter.setTimeZone(TimeZone.getTimeZone(timezone));
		strDate[0] = formatter.format(date);
		
		formatter = new SimpleDateFormat("dd");  
		formatter.setTimeZone(TimeZone.getTimeZone(timezone));
		strDate[1] = formatter.format(date);
		todayDate = strDate[1];
		
		formatter = new SimpleDateFormat("MM");  
		formatter.setTimeZone(TimeZone.getTimeZone(timezone));
		strDate[2] = formatter.format(date);
		month = strDate[2];
		
		formatter = new SimpleDateFormat("yyyy");  
		formatter.setTimeZone(TimeZone.getTimeZone(timezone));
		strDate[3] = formatter.format(date);
		year = strDate[3];
		
		return strDate;
	}
	
	public static int getScrolledThumbnailCount(String lastThumbnailXpath, String secondLastThumbnailXpath, int[] thumbnailSwipeCoord) throws InterruptedException {
		
		boolean secondThumbCheck = false;
		for(int i = 0; i < 4; i ++) {
			int check1 = driver.findElementsByXPath(lastThumbnailXpath).size();
			int check2 = driver.findElementsByXPath(secondLastThumbnailXpath).size();
			if(check1 == 0) {
				if(check2 == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image found");
					lastThumbnailXpath = secondLastThumbnailXpath;
					secondThumbCheck = true;
					break;
				}
			} else {
				log.info("Image found");
				break;
			}
		}
		
		String text1 = driver.findElementByXPath(lastThumbnailXpath).getText().replaceAll(" ", "");

		int count = Integer.parseInt(text1);

		int imagesCount = 0;

		if(count != 1) {
			for(int j = 0; j < 20; j++) {
				for(int i = 0; i < 6; i++) {

					(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
					.release().perform();

				}

				Thread.sleep(5000);
				
				String text = null;
				try {
					text = driver.findElementByXPath(lastThumbnailXpath).getText().replaceAll(" ", "");
				} catch (NoSuchElementException e) {

					(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
					.release().perform();
				}
				try {
					imagesCount = Integer.parseInt(text);
				} catch (NumberFormatException n) {
					log.info("NumberFormatException in text " + text);
					
					(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
					.release().perform();
					
					Thread.sleep(7000);
					
					try {
						text = driver.findElementByXPath(lastThumbnailXpath).getText().replaceAll(" ", "");
					} catch (NoSuchElementException n1) {
						text = driver.findElementByXPath(secondLastThumbnailXpath).getText().replaceAll(" ", "");
					}
					
					imagesCount = Integer.parseInt(text);
				}
				if(imagesCount == count && (secondThumbCheck == true)) {
					imagesCount = imagesCount + 1;
					break;
				} else if(imagesCount == count && (secondThumbCheck == false)) {
					break;
				}
				else count = imagesCount;
			}
		}
		return imagesCount;
	}
	
	/* Moving a file from particular source to destination */
	public static void moveFile(File source, File destination) throws Exception {

		try {
			FileUtils.moveFile(source, destination);
			log.info("movedfile to shared drive");
			sharedDriveFileLocation = String.valueOf(destination).replace("\\", "/");
			if(sharedDriveFileLocation.startsWith("//")) {
				sharedDriveFileLocation = sharedDriveFileLocation.replace("//", "");
			}
			status = "SHARED DRIVE UPLOAD - PASSED";
			isUploadedInSharedDrive = "UPLOADED";
		} catch (FileExistsException e1) {
			log.info("Exception--" + e1);
			log.info("File already exist in sharedrive");
			sharedDriveFileLocation = String.valueOf(destination).replace("\\", "/");
			if(sharedDriveFileLocation.startsWith("//")) {
				sharedDriveFileLocation = sharedDriveFileLocation.replace("//", "");
			}
			status = "SHARED DRIVE UPLOAD - PASSED";
			isUploadedInSharedDrive = "UPLOADED";
		} catch (Exception e) {
			log.info("Exception--" + e);
			log.info("Error! Fail Job!");
			throw e;
		}
	}
	
	/*End script called by finally block of each Job */
	public static void endJob() throws IOException {
		APIdbRelated.dbInsertion();
		driver.quit();
	}
}
