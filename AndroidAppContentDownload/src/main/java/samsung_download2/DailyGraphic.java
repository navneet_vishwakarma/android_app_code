package samsung_download2;

import java.time.Duration;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * DailyGraphic --- program to download DailyGraphic app publications
 * @author NA20251768
 *
 */
public class DailyGraphic extends AppRelated {

	/*	

	Daily Graphic ----- DONE

	 */

	static int[] swipeCoordinates = {900, 992, 134, 988};
	static int[] thumbnailSwipeCoord = {1087, 1784, 138, 1784};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(DailyGraphic.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			
			MobileElement el1 = (MobileElement) driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
			el1.click();
			MobileElement el2 = (MobileElement) driver.findElementByAccessibilityId("Profile");
			el2.click();
			MobileElement el3 = (MobileElement) driver.findElementById("com.graphicnation:id/login");
			el3.click();
			MobileElement el4 = (MobileElement) driver.findElementById("com.graphicnation:id/email");
			el4.sendKeys(username());
			MobileElement el5 = (MobileElement) driver.findElementById("com.graphicnation:id/pass");
			el5.sendKeys(password());
			MobileElement el6 = (MobileElement) driver.findElementById("com.graphicnation:id/btnlogin");
			el6.click();

			Thread.sleep(5000);
			
			int outerLoop = driver.findElementsByXPath("//android.widget.TextView[@resource-id='com.graphicnation:id/textEpaper']/../android.widget.LinearLayout[@resource-id='com.graphicnation:id/redLayout']/android.widget.LinearLayout").size();
			for(int i = 1; i <= outerLoop; i++) {
				int innerLoop = driver.findElementsByXPath("//android.widget.TextView[@resource-id='com.graphicnation:id/textEpaper']/../android.widget.LinearLayout[@resource-id='com.graphicnation:id/redLayout']/android.widget.LinearLayout[" + i + "]/android.widget.ImageView").size();
				for(int j = 1; j <= innerLoop; j++) {
					MobileElement el7 = driver.findElementByXPath("//android.widget.TextView[@resource-id='com.graphicnation:id/textEpaper']/../android.widget.LinearLayout[@resource-id='com.graphicnation:id/redLayout']/android.widget.LinearLayout[" + i + "]/android.widget.ImageView[" + j + "]");
					el7.click();
					String text = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='com.graphicnation:id/toolbar_fullepaper']/android.widget.TextView").getAttribute("text");
					log.info(text + " : " + keyWord);
					if(text.contains(keyWord)) {
						break;
					} else {
						driver.navigate().back();
					}
				}
			}
			
			MobileElement el11 = (MobileElement) driver.findElementById("com.graphicnation:id/upslider");
			el11.click();
			
			String date = driver.findElementByXPath("//android.widget.Spinner[@resource-id='com.graphicnation:id/spinner2']/android.widget.TextView").getText();
			log.info(date);

			String lastThumbnailXpath = "//android.support.v7.widget.RecyclerView[@resource-id='com.graphicnation:id/recycler']/android.widget.LinearLayout[last()-1]/android.widget.TextView";
			String secondLastThumbnailXpath = "//android.support.v7.widget.RecyclerView[@resource-id='com.graphicnation:id/recycler']/android.widget.LinearLayout[last()-1]/android.widget.TextView";
			
			boolean secondThumbCheck = false;
			for(int i = 0; i < 4; i ++) {
				int check1 = driver.findElementsByXPath(lastThumbnailXpath).size();
				int check2 = driver.findElementsByXPath(secondLastThumbnailXpath).size();
				if(check1 == 0) {
					if(check2 == 0) {
						log.info("Loaded again...");
					} else {
						log.info("Image found");
						lastThumbnailXpath = secondLastThumbnailXpath;
						secondThumbCheck = true;
						break;
					}
				} else {
					log.info("Image found");
					break;
				}
			}
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getText().substring(5);

			log.info(text1 + " = text1");
			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 6; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);
					
					String text = null;
					try {
						text = driver.findElementByXPath(lastThumbnailXpath).getText().substring(5);
						log.info(text1 + " = text2");
					} catch (NoSuchElementException e) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
					}
					try {
						imagesCount = Integer.parseInt(text);
					} catch (NumberFormatException n) {
						log.info("NumberFormatException in text " + text);
						
						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
						
						Thread.sleep(7000);
						
						try {
							text = driver.findElementByXPath(lastThumbnailXpath).getText().substring(5);
							log.info(text + " = text");
						} catch (NoSuchElementException n1) {
							text = driver.findElementByXPath(secondLastThumbnailXpath).getText().substring(5);
							log.info(text + " = text");
						}
						
						imagesCount = Integer.parseInt(text);
					}
					if(imagesCount == count && (secondThumbCheck == true)) {
						imagesCount = imagesCount + 1;
						break;
					} else if(imagesCount == count && (secondThumbCheck == false)) {
						break;
					}
					else count = imagesCount;
				}
			}
			
			log.info("Images count " + imagesCount);
			Thread.sleep(3000);
			
			el11 = (MobileElement) driver.findElementById("com.graphicnation:id/upslider");
			el11.click();
			
			Thread.sleep(3000);
			
			tearDownWait(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}

}
