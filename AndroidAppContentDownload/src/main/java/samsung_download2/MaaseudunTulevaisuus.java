package samsung_download2;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Story --- program to download Story app publications
 * @author NA20251768
 *
 */
public class MaaseudunTulevaisuus extends AppRelated {

	/*	

	Maaseudun Tulevaisuus ----- DONE
	
	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {1137, 1703, 276, 1703};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(MaaseudunTulevaisuus.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			Thread.sleep(10000);

			MobileElement el1 = driver.findElementByXPath("//android.widget.ListView/android.view.View[2]/android.view.View/android.view.View[3]/android.view.View");
			String date = el1.getText();
			log.info(date);
			el1.click();
			
			Thread.sleep(5000);
						
			MobileElement els2 = driver.findElementByXPath("//android.widget.Button[@resource-id='onetrust-accept-btn-handler']");
			els2.click();
			MobileElement els3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='Username']");
			els3.sendKeys(username());
			MobileElement els4 = driver.findElementByXPath("//android.widget.EditText[@resource-id='Password']");
			els4.sendKeys(password());
			MobileElement els5 = driver.findElementByXPath("//android.widget.Button[@text='Kirjaudu']");
			els5.click();
			Thread.sleep(25000);
			MobileElement els6 = driver.findElementByXPath("//android.view.View[@resource-id='fi.viestilehdet.MT:id/handle_click_area']");
			els6.click();
			
			
			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[last()]//android.widget.TextView[@resource-id='fi.viestilehdet.MT:id/left_text']";
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath(lastThumbnailXpath).size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			int count = Integer.parseInt(text1.substring(5));

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 10; j++) {
					for(int i = 0; i < 5; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
					imagesCount = Integer.valueOf(text.substring(5));

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}

			log.info("The images count is " + imagesCount);
			
			MobileElement els12 = driver.findElementByXPath("//androidx.viewpager.widget.ViewPager[@resource-id='fi.viestilehdet.MT:id/viewpager']");
			els12.click();
			
			Thread.sleep(4000);
			
			els12.click();
			
			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}

}
