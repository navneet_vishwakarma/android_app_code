package samsung_download2;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * CourrierPicard --- program to download CourrierPicard app publications
 * @author NA20251768
 *
 */
public class CourrierPicard extends AppRelated {

	/*	

	Le courier Picard � Oise
	Le Courrier Picard - Amiens ----- DONE

	 */

	static int[] swipeCoordinates = {1449, 1127, 113, 1127};
	static int[] thumbnailSwipeCoord = {1349, 2270, 1349, 448};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(CourrierPicard.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/button_agree");
			el1.click();
			MobileElement el2 = driver.findElementByAccessibilityId("Ouvrir le menu");
			el2.click();
			MobileElement el3 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/textView_login");
			el3.click();
			Thread.sleep(3000);
			//MobileElement el4 = driver.findElementById("android:id/autofill_dataset_list");
			//el4.click();
			//Thread.sleep(3000);
			MobileElement el5 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/editText_email");
			el5.clear();
			el5.sendKeys(username());
			MobileElement el6 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/editText_password");
			el6.sendKeys(password());
			MobileElement el7 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/button_login");
			el7.click();
			
			Thread.sleep(7000);
			driver.navigate().back();

			/*
			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByAccessibilityId("Navigate up").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			
			MobileElement el9 = driver.findElementByAccessibilityId("Navigate up");
			el9.click();
			*/
			MobileElement el10 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/actionNewspaper");
			el10.click();
			Thread.sleep(3000);
			MobileElement el11 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/actionEditions");
			el11.click();
			Thread.sleep(3000);
			MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'" + keyWord + "')]");
			els1.click();
			MobileElement el12 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/md_buttonDefaultPositive");
			el12.click();
			MobileElement els2 = driver.findElementByXPath("//android.widget.ListView/android.widget.LinearLayout[1]//android.widget.TextView[@resource-id='com.audaxis.mobile.courrierpicard:id/textView_date']");
			String date = els2.getText();

			MobileElement el13 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/button_download");
			el13.click();
			
			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsById("com.audaxis.mobile.courrierpicard:id/button_cancel").size();
				if(check == 0) {
					log.info("Loaded again...");
					try {
					el13 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/button_download");
					el13.click();
					} catch(Exception e) {
						
					}
				} else {
					log.info("Cancel image button found");
					
					break;
				}
			}
			
			for(int i = 0; i < 11; i++) {
				int check = driver.findElementsById("com.audaxis.mobile.courrierpicard:id/button_cancel").size();
				if(check == 1) {
					log.info("For image Load again...");
					try {
						el13 = driver.findElementById("com.audaxis.mobile.courrierpicard:id/button_download");
						el13.click();
						} catch(Exception e) {
							
						}
				} else {
					log.info("Image found");
					break;
				}
			}
		
			MobileElement el15 = driver.findElementByAccessibilityId("Sommaire");
			el15.click();

			String lastThumbnailXpath = "//android.widget.ExpandableListView/android.widget.LinearLayout[last()]//android.widget.TextView";
			
			Thread.sleep(3000);
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
					imagesCount = Integer.parseInt(text);

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}

			log.info("The images count is " + imagesCount);	

			el15 = driver.findElementByAccessibilityId("Sommaire");
			el15.click();
			
			tearDown(imagesCount + 1, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}

}
