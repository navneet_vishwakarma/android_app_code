package samsung_download2;

import java.io.File;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;
import utility.ImagesPDFrelated;
import utility.ReadWrite;

/**
 * LaFranceAgricole --- program to download LaFranceAgricole app publications
 * @author NA20251768
 *
 */
public class LaFranceAgricole extends AppRelated {

	/*	

	La France Agricole ----- DONE

	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(LaFranceAgricole.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByXPath("//android.view.View[@resource-id='contents']/android.view.View[contains(@resource-id,'La France Agricole---FA')][1]/android.view.View[last()]").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			
			String date = driver.findElementByXPath("//android.view.View[@resource-id='contents']/android.view.View[contains(@resource-id,'La France Agricole---FA')][1]/android.view.View[last()]").getText();
			log.info(date);
			
			MobileElement els2 = driver.findElementByXPath("//android.view.View[@resource-id='contents']/android.view.View[contains(@resource-id,'La France Agricole---FA')][1]//android.widget.Image");
			els2.click();
			MobileElement els3 = driver.findElementByXPath("//android.view.View[@resource-id='signinform']/android.view.View/android.widget.EditText[1]");
			els3.sendKeys(username());
			MobileElement els4 = driver.findElementByXPath("//android.view.View[@resource-id='signinform']/android.view.View/android.widget.EditText[2]");
			els4.sendKeys(password());
			MobileElement els5 = driver.findElementByXPath("//android.view.View[@resource-id='signinform']/android.view.View/android.view.View[@text='VALIDER']");
			els5.click();
			Thread.sleep(7000);
			MobileElement els6 = driver.findElementByXPath("//android.view.View[@resource-id='contents']/android.view.View[contains(@resource-id,'La France Agricole---FA')][1]//android.widget.Image");
			els6.click();

			for(int i = 0; i < 4; i++) {
				int check = driver.findElementsByXPath("//android.view.View[starts-with(@resource-id,'p')]").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			img = new ImagesPDFrelated();

			img.deleteFiles(app, keyWord);

			Thread.sleep(10000);

			int imagesCount = 2;
			
			rw = new ReadWrite();
			TakesScreenshot sc = driver;
			File src = sc.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P1.png"));
			log.info(1 + " screenshot taken");
			
			while(imagesCount < 1000) {

				Thread.sleep(2000);
				
				(new AndroidTouchAction(driver)).press(PointOption.point(swipeCoordinates[0], swipeCoordinates[1]))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(swipeCoordinates[2], swipeCoordinates[3]))
				.release().perform();
				
				Thread.sleep(2000);
				
				try {
					driver.findElementByXPath("//android.view.View[@resource-id='p"+imagesCount+"']");
				} catch (Exception e) {
					break;
				}
				Thread.sleep(2000);

				sc = driver;
				src = sc.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(src, new File(rw.readProperties().getProperty("captureRootPath") + File.separator + app + File.separator + keyWord + "/P" + imagesCount +".png"));
				log.info(imagesCount + " screenshot taken");
				imagesCount++;
			}

			img.generatePDF(date, keyWord, app);
			
			log.info("The images count is " + imagesCount);
		
		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}

}
