package samsung_download2;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Story --- program to download Story app publications
 * @author NA20251768
 *
 */
public class Story extends AppRelated {

	/*	

	Story ----- DONE
	
	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {616, 1715, 616, 361};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Story.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementById("be.persgroep.storyreplica:id/accept_button");
			el1.click();
			
			Thread.sleep(5000);
			
			driver.navigate().back();
			
			MobileElement el2 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//android.widget.TextView[@resource-id='be.persgroep.storyreplica:id/kiosk_frontpage_date']");
			String date = el2.getText();
			log.info(date);
			el2.click();
			
			Thread.sleep(7000);
			
			MobileElement el21 = driver.findElementById("be.persgroep.storyreplica:id/archive_fab");
			el21.click();
			MobileElement el22 = driver.findElementById("android:id/button1");
			el22.click();
			
			el2 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]//android.widget.TextView[@resource-id='be.persgroep.storyreplica:id/kiosk_frontpage_date']");
			el2.click();
			
			for(int i = 0; i < 25; i++) {
				int check = driver.findElementsByAccessibilityId("Pages").size();
				if(check != 0) {
					log.info("Element found");
					break;
				}
			}
			
			MobileElement el3 = driver.findElementByAccessibilityId("Pages");
			el3.click();
						
			String text = null;
			for(int i = 0; i < 10; i++) {
				try {
					MobileElement el4 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup//android.widget.TextView[@resource-id='be.persgroep.storyreplica:id/pages_overview_left_textview']");
					text = el4.getText();
					break;
				} catch (Exception e) {
					log.info("Try again");
				}
				for(int j = 0; j < 7; j++) {
					(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
					.release().perform();
				}
			}
			
			int imagesCount = Integer.parseInt(text.substring(text.lastIndexOf(" ") + 1));
			log.info("Images count = " + imagesCount);
			
			driver.navigate().back();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			endJob();
		}
	}

}
