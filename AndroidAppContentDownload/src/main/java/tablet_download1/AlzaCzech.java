package tablet_download1;

import java.time.Duration;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Alza --- program to download Alza app publications
 * @author NA20251768
 *
 */
public class AlzaCzech extends AppRelated {

	/*
Pravo ----- DONE
Svet motoru----- DONE
Marketing & Media ----- DONE
Reflex ----- DONE
Sedmicka ----- DONE
Orlicky tydenik ----- DONE
Muj kousek stesti ----- DONE
Katolicky tydenik ----- DONE
Moje sladke tajemstvi ----- DONE
Muj cas na kaficko ----- DONE
Moje chvilka pohody ----- DONE
Horacke noviny ----- DONE
Kvety ----- DONE
Tema ----- DONE
Euro ----- DONE
Nedelni Aha ----- DONE
Nedelni Blesk ----- DONE
Blesk pro zeny ----- DONE
Aha pro zeny ----- DONE


	 */

	static int[] swipeCoordinates = {1043, 964, 170, 958};
	static int[] thumbnailSwipeCoord = {603, 1417, 638, 408};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(AlzaCzech.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement els1 = driver.findElementById("cz.alza.media:id/czech_iv");
			els1.click();
			//MobileElement els2 = driver.findElementByXPath("//android.widget.Button[@resource-id='cz.alza.media:id/choose_shop_btn']");
			//Sels2.click();
			MobileElement els3 = driver.findElementByXPath("//android.widget.Button[@text='OK']");
			els3.click();
			MobileElement els5 = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='cz.alza.media:id/close_btn']");
			els5.click();
			//MobileElement el1 = driver.findElementByAccessibilityId("Menu");
			//el1.click();
			MobileElement els7 = driver.findElementByXPath("//android.widget.TextView[contains(@text,'" + rw.excelReader(app, keyWord, "testData1") + "')]");
			els7.click();
			MobileElement els9 = driver.findElementByXPath("//android.widget.EditText[@resource-id='userName']");
			els9.sendKeys(username());
			MobileElement els10 = driver.findElementByXPath("//android.widget.EditText[@resource-id='password']");
			els10.sendKeys(password());
			MobileElement els11 = driver.findElementByXPath("//android.widget.Button[@resource-id='btnLogin']");
			els11.click();
			Thread.sleep(7000);
			MobileElement els111 = driver.findElementByXPath("//android.widget.ImageButton");
			els111.click();
			Thread.sleep(2000);
			MobileElement el3 = driver.findElementByXPath("//android.widget.TextView[@text='" + rw.excelReader(app, keyWord, "testData2") + "']");
			el3.click();			
			for(int i = 0; i < 100; i ++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='" + rw.excelReader(app, keyWord, "testData4") + "']").size();
				if(check == 0) {
					log.info("Scroll down ...");
					(new AndroidTouchAction(driver)).press(PointOption.point(628, 1662))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(628, 700))
					.release().perform();
				} else {
					log.info("Publication found");
					break;
				}
			}
			//android.widget.TextView[@text='Marketing & Media']

			MobileElement els12 = driver.findElementByXPath("//android.widget.TextView[@text='" + rw.excelReader(app, keyWord, "testData4") + "']");
			els12.click();
			Thread.sleep(5000);

			/*
			MobileElement els17 = driver.findElementByXPath("//android.widget.GridView[@resource-id='cz.alza.media:id/library_list']//android.widget.LinearLayout[@resource-id='cz.alza.media:id/root_item_view'][1]//android.widget.TextView[@resource-id='cz.alza.media:id/name_tv']");
			String date = els17.getText().replaceAll("[-()?:!.,;{}]+", "/");
						
			if(!AppRelated.checkDateGreaterThanCurrent(date)) {
				MobileElement els18 = driver.findElementByXPath("//android.widget.GridView[@resource-id='cz.alza.media:id/library_list']//android.widget.LinearLayout[@resource-id='cz.alza.media:id/root_item_view'][2]//android.widget.TextView[@resource-id='cz.alza.media:id/name_tv']");
				date = els18.getText().replaceAll("[-()?:!.,;{}]+", "/");
				els18.click();
			} else {
				els17.click();
			}
			*/
			
			MobileElement els17 = driver.findElementByXPath("//android.widget.GridView[@resource-id='cz.alza.media:id/library_list']/android.widget.LinearLayout[1]//android.widget.TextView");
			
			//MobileElement els17 = driver.findElementByXPath("//android.widget.GridView[@resource-id='cz.alza.media:id/library_list']//android.widget.LinearLayout[@resource-id='cz.alza.media:id/root_item_view'][2]//android.widget.TextView[@resource-id='cz.alza.media:id/name_tv']");
			String date = els17.getText();
			log.info(date);
			els17.click();
			
			for(int i = 0; i < 20; i ++) {
				int check = driver.findElementsById("cz.alza.media:id/glasses_iv").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			
			MobileElement els19 = driver.findElementById("cz.alza.media:id/glasses_iv");
			els19.click();
			//MobileElement els20 = driver.findElementByAccessibilityId(rw.excelReader(app, keyWord, "testData3"));
			MobileElement els20 = driver.findElementByXPath("//android.widget.TextView[@resource-id='cz.alza.media:id/pspdf__menu_option_thumbnail_grid']");

			els20.click();
			
			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.RelativeLayout[last()]";
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
			text1 = text1.substring(text1.indexOf(" ") + 1);
			int count = Integer.parseInt(text1);
			System.out.println("Count 1 = " + count);

			int imagesCount = 0;
			
			if(count != 1) {
				for(int j = 0; j < 10; j++) {
					for(int i = 0; i < 5; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);
					
					String text = null;
					try {
						text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
						text = text.substring(text.indexOf(" ") + 1);
						System.out.println("Count 2 = " + text);
					} catch (NoSuchElementException e) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
					}
					try {
						imagesCount = Integer.parseInt(text);
					} catch (NumberFormatException n) {
						log.info("NumberFormatException in text " + text);
						
						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();
						
						Thread.sleep(7000);
						
						text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("content-desc");
						text = text.substring(text.indexOf(" ") + 1);
						System.out.println("Count 3 = " + text);
						imagesCount = Integer.parseInt(text);
					}
					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}
		
			log.info("The images count is " + imagesCount);
			
			els20 = driver.findElementByXPath("//android.widget.TextView[@resource-id='cz.alza.media:id/pspdf__menu_option_thumbnail_grid']");
			els20.click();

			MobileElement els21 = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='cz.alza.media:id/pspdf__document_view']//android.view.View[@index=0]");
			els21.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}
}
