package tablet_download1;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import utility.AppRelated;

/**
 * Pocketmags --- program to download Pocketmags app publications
 * @author NA20251768
 *
 */
public class Pocketmags extends AppRelated {

	/*

Closer ----- FF ----- DONE
Chat ----- FV ----- DONE

	 */

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1103, 1621, 217, 1621};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Pocketmags.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			int check = driver.findElementsById("com.triactivemedia.pocketmags:id/pm_onboarding_button").size();
			if(check != 0) {
				MobileElement el1 = driver.findElementById("com.triactivemedia.pocketmags:id/pm_onboarding_button");
				el1.click();
				MobileElement el2 = driver.findElementByAccessibilityId("Options");
				el2.click();
				MobileElement els1 = driver.findElementByXPath("//android.widget.TextView[@text='Login / Register to Pocketmags']");
				els1.click();
				MobileElement el4 = driver.findElementById("com.triactivemedia.pocketmags:id/login_textinput_email");
				el4.sendKeys(username());
				MobileElement el5 = driver.findElementById("com.triactivemedia.pocketmags:id/login_radio_existing");
				el5.click();
				MobileElement el6 = driver.findElementById("com.triactivemedia.pocketmags:id/login_textinput_password1");
				el6.sendKeys(password());
				MobileElement el7 = driver.findElementById("com.triactivemedia.pocketmags:id/login_button_nextstep");
				el7.click();
			}
			MobileElement el8 = driver.findElementByAccessibilityId("Browse");
			el8.click();
			MobileElement el9 = driver.findElementById("com.triactivemedia.pocketmags:id/search_src_text");
			el9.sendKeys(keyWord);
			MobileElement els2 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.ImageView");
			els2.click();
			String date = null;
			for(int i = 0; i < 4; i++) {
				int check1 = driver.findElementsById("com.triactivemedia.pocketmags:id/titlepage_textview_issue_name").size();
				if(check1 != 0) {
					date = driver.findElementById("com.triactivemedia.pocketmags:id/titlepage_textview_issue_name").getText();
					log.info(date);
					break;
				}
			}
			
			MobileElement el10 = driver.findElementById("com.triactivemedia.pocketmags:id/titlepage_button_issue");
			el10.click();
			MobileElement els5 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.view.View");
			els5.click();
			
			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_framelayout_root'][last()]//android.widget.TextView[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_textview_pagenumber']";
			String secondLastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.FrameLayout[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_framelayout_root'][last()-1]//android.widget.TextView[@resource-id='com.triactivemedia.pocketmags:id/thumbnails_textview_pagenumber']";

			int imagesCount = getScrolledThumbnailCount(lastThumbnailXpath, secondLastThumbnailXpath, thumbnailSwipeCoord);

			log.info("The images count is " + imagesCount);

			MobileElement el14 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.view.View");
			el14.click();

			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}

}
