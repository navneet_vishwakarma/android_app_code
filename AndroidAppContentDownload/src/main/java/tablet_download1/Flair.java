package tablet_download1;

import java.time.Duration;

import org.apache.log4j.Logger;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Flair --- program to download Flair app publications
 * @author NA20251768
 *
 */
public class Flair extends AppRelated {

	static int[] swipeCoordinates = {1099, 930, 151, 930};
	static int[] thumbnailSwipeCoord = {1106, 1737, 226, 1737};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Flair.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);
			
			MobileElement el1 = driver.findElementById("be.appsolution.flairnl.tablet:id/toolbar_login");
			el1.click();
			
			MobileElement el2 = driver.findElementByXPath("//android.widget.EditText[@resource-id='capture_signIn_signInEmailAddress']");
			el2.sendKeys(username());
			
			MobileElement el3 = driver.findElementByXPath("//android.widget.EditText[@resource-id='capture_signIn_currentPassword']");
			el3.sendKeys(password());

			(new AndroidTouchAction(driver)).press(PointOption.point(550, 1213))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(550, 886))
			.release().perform();
			
			MobileElement els4 = driver.findElementByXPath("//android.widget.Button[@resource-id='signInSubmit']");
			els4.click();
			
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsById("be.appsolution.flairnl.tablet:id/lees_nu_button").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			MobileElement el5 = driver.findElementById("be.appsolution.flairnl.tablet:id/lees_nu_button");
			el5.click();
			
			MobileElement el51 = driver.findElementByXPath("//android.widget.GridView[@resource-id='be.appsolution.flairnl.tablet:id/kiosk_gridView']/android.widget.LinearLayout[@resource-id='be.appsolution.flairnl.tablet:id/kiosk_grid_item'][1]//android.widget.TextView[@resource-id='be.appsolution.flairnl.tablet:id/grid_date']");
			String date = el51.getText();
			log.info(date);
			
			el51.click();
			
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("//android.widget.Button[@resource-id='be.appsolution.flairnl.tablet:id/pagina_up']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}
			
			
			MobileElement el6 = driver.findElementByXPath("//android.widget.Button[@resource-id='be.appsolution.flairnl.tablet:id/pagina_up']");
			el6.click();
			
			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.widget.LinearLayout[last()]//android.widget.TextView";
			
			String text1 = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			int count = Integer.parseInt(text1);

			int imagesCount = 0;

			if(count != 1) {
				for(int j = 0; j < 20; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(5000);

					String text = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");
					imagesCount = Integer.parseInt(text);

					if(imagesCount == count)
						break;
					else count = imagesCount;
				}
			}

			log.info("The images count is " + imagesCount);	
			
			MobileElement el13 = driver.findElementByXPath("//android.view.View[@resource-id='be.appsolution.flairnl.tablet:id/view']");
			el13.click();
			
			tearDown(imagesCount, date, keyWord, app, swipeCoordinates);
			
		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}

	}
}
