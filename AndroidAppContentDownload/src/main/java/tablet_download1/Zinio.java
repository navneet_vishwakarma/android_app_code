package tablet_download1;

import java.time.Duration;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.KeyInput;
import org.openqa.selenium.interactions.Sequence;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utility.AppRelated;

/**
 * Zinio --- program to download Zinio app publications
 * @author NA20251768
 *
 */
public class Zinio extends AppRelated {

	/*	
 	mediamanagement@prime-research.com, Mainz2014!

	Hola ----- FF ----- DONE
	Ude & Hjemme ----- VV ----- DONE
	Sondag ----- VV ----- DONE
	femina Denmark ----- VV ----- DONE

	a2subscriptions@prime-research.com,	Maynard309

	The New Yorker ----- FF ----- DONE
	Billboard ----- FF ----- DONE
	AdWeek ----- FF ----- DONE
	that's life! ----- VV ----- DONE
	Hollywood Reporter ----- FV ----- DONE

	uk.media@prime-research.com, Oxford2015

	Sport's Illustrated ----- DONE
	Publisher Weekly ----- DONE

	 */

	static int[] swipeCoordinates = {1128, 820, 88, 820};
	static int[] thumbnailSwipeCoord = {1093, 1706, 188, 1706};

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(Zinio.class.getName());

	public static void main(String[] args) throws Exception {

		try {
			driver = setUp(args);

			MobileElement el1 = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]");
			el1.click();

			MobileElement el2 = driver.findElementById("com.zinio.mobile.android.reader:id/btn_start");
			el2.click();

			Thread.sleep(7000);

			MobileElement el3 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Profile\"]/android.view.ViewGroup/android.widget.TextView");
			el3.click();

			MobileElement el4 = driver.findElementByXPath("//android.widget.TextView[@text='Sign In']");
			el4.click();

			MobileElement el5 = driver.findElementByXPath("//android.widget.Button[@text='SIGN IN']");
			el5.click();

			MobileElement el6 = driver.findElementById("com.zinio.mobile.android.reader:id/email_field");
			el6.sendKeys(username());

			MobileElement el7 = driver.findElementById("com.zinio.mobile.android.reader:id/password_field");
			el7.sendKeys(password());

			MobileElement el8 = driver.findElementById("com.zinio.mobile.android.reader:id/sign_in_button");
			el8.click();

			MobileElement el9 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Search\"]/android.view.ViewGroup/android.widget.TextView");
			el9.click();

			MobileElement el10 = driver.findElementById("com.zinio.mobile.android.reader:id/search_src_text");
			el10.clear();
			el10.sendKeys(keyWord);
			el10.click();

			Thread.sleep(5000);

			KeyInput keyboard = new KeyInput("keyboard");
			Sequence sendKeys = new Sequence(keyboard, 0);
			sendKeys.addAction(keyboard.createKeyDown(Keys.ENTER.getCodePoint()));

			driver.perform(Arrays.asList(sendKeys));

			Thread.sleep(5000);

			MobileElement el14 = driver.findElementByXPath("//androidx.cardview.widget.CardView[1]//android.widget.ImageView[@resource-id='com.zinio.mobile.android.reader:id/iv_cover']");
			el14.click();

			Thread.sleep(7000);

			String date = driver.findElementById("com.zinio.mobile.android.reader:id/issue_name").getText();
			log.info(date);

			MobileElement el15 = driver.findElementById("com.zinio.mobile.android.reader:id/single_issue_button");
			el15.click();

			String lastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@resource-id='com.zinio.mobile.android.reader:id/pages_portrait_container'][last()]//android.widget.TextView[@resource-id='com.zinio.mobile.android.reader:id/tv_folio_number']";

			String secondlastThumbnailXpath = "//androidx.recyclerview.widget.RecyclerView//android.view.ViewGroup[@resource-id='com.zinio.mobile.android.reader:id/pages_portrait_container'][last()-1]//android.widget.TextView[@resource-id='com.zinio.mobile.android.reader:id/tv_folio_number']";

			/*
			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath("//android.widget.TextView[@text='PDF']").size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					MobileElement el16 = driver.findElementByXPath("//android.widget.TextView[@text='PDF']");
					el16.click();
					break;
				}
			}
			*/

			for(int i = 0; i < 10; i ++) {
				int check = driver.findElementsByXPath(lastThumbnailXpath).size();
				if(check == 0) {
					log.info("Loaded again...");
				} else {
					log.info("Image opened");
					break;
				}
			}

			String count = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

			String textCount = null;


			if(!count.equals("C4")) {
				for(int j = 0; j < 5; j++) {
					for(int i = 0; i < 10; i++) {

						(new AndroidTouchAction(driver)).press(PointOption.point(thumbnailSwipeCoord[0], thumbnailSwipeCoord[1]))
						.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
						.moveTo(PointOption.point(thumbnailSwipeCoord[2], thumbnailSwipeCoord[3]))
						.release().perform();

					}

					Thread.sleep(10000);

					textCount = driver.findElementByXPath(lastThumbnailXpath).getAttribute("text");

					if(textCount.equals(count))
						break;
					else count = textCount;
				}
			}

			String text = driver.findElementByXPath(secondlastThumbnailXpath).getAttribute("text");

			int imagesCount = Integer.parseInt(text);

			log.info("The images count is " + (imagesCount + 1));

			MobileElement el12 = driver.findElementById("com.zinio.mobile.android.reader:id/pdf_reader_container_coordinator");
			el12.click();

			tearDown(imagesCount + 1, date, keyWord, app, swipeCoordinates);

			/*
			driver.navigate().back();

			driver.navigate().back();

			el3 = driver.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Profile\"]/android.view.ViewGroup/android.widget.TextView");
			el3.click();

			MobileElement el13 = driver.findElementByXPath("//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.zinio.mobile.android.reader:id/icon']");
			el13.click();

			MobileElement el140 = driver.findElementByXPath("//android.widget.TextView[@text='Sign Out']");
			el140.click();

			MobileElement el150 = driver.findElementByXPath("//android.widget.Button[@text='OK']");
			el150.click();
			*/

		} catch (Exception e) {
			handleException(e);
		} finally {
			driver.quit();
		}
	}

}
