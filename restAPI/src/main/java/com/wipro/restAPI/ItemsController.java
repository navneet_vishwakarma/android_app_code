package com.wipro.restAPI;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemsController {

	@Autowired
	private ItemsService service;
	
	@GetMapping("/transactions")
	public List<TblAndroidTransaction> list() {
		return service.listAll();
	}
	
	@GetMapping("/transactions/{id}")
	public ResponseEntity<TblAndroidTransaction> get(@PathVariable Integer id) {
		try {
			TblAndroidTransaction tblAndroidTransaction = service.get(id);
			return new ResponseEntity<TblAndroidTransaction>(tblAndroidTransaction, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<TblAndroidTransaction>(HttpStatus.NOT_FOUND);
		}		
	}
	
	@PostMapping("/transactions")
	public void add(@RequestBody TblAndroidTransaction tblAndroidTransaction) {
		service.save(tblAndroidTransaction);
	}
	
	@PutMapping("/transactions/{id}")
	public ResponseEntity<?> update(@RequestBody TblAndroidTransaction tblAndroidTransaction,
			@PathVariable Integer id) {
		try {
			TblAndroidTransaction existItems = service.get(id);
			service.save(tblAndroidTransaction);
			
			return new ResponseEntity<TblAndroidTransaction>(tblAndroidTransaction, HttpStatus.OK);
			
		} catch (NoSuchElementException e) {
			return new ResponseEntity<TblAndroidTransaction>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/transactions/{id}")
	public void delete(@PathVariable Integer id) {
		service.delete(id);		
	}
}
