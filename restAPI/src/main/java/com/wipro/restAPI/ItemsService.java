package com.wipro.restAPI;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemsService {

	@Autowired
	private ItemsRepository repo;
	
	public List<TblAndroidTransaction> listAll() {
		return repo.findAll();
	}
	
	public void save(TblAndroidTransaction tblAndroidTransaction) {
		repo.save(tblAndroidTransaction);
	}
	
	public TblAndroidTransaction get(Integer androidTransactionID) {
		return repo.findById(androidTransactionID).get();
	}
	
	public void delete(Integer androidTransactionID) {
		repo.deleteById(androidTransactionID);
	}
}
