package com.wipro.restAPI;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tblAndroidTransaction")
public class TblAndroidTransaction {

	@Id
	@Column(name = "AndroidTransactionId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer androidTransactionId;
	
	@Column(name = "MediaId")
	private String mediaId;
	
	@Column(name = "DeviceId")
	private String deviceId;
	
	@Column(name = "Status")
	private String status;
	
	@Column(name = "DurationInSec")
	private Integer durationInSec;
	
	@Column(name = "SDUploadStatus")
	private String sDUploadStatus;
	
	@Column(name = "DownloadedDate")
	private String downloadedDate;
	
	@Column(name = "SharedDriveFileLocation")
	private String sharedDriveFileLocation;
	
	@Column(name = "HorreumUploadStatus")
	private String horreumUploadStatus;
	
	@Column(name = "DownloadedFileName")
	private String downloadedFileName;
	
	public Integer getId() {
		return androidTransactionId;
	}
	public void setId(Integer androidTransactionId) {
		this.androidTransactionId = androidTransactionId;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getDurationInSec() {
		return durationInSec;
	}
	public void setDurationInSec(Integer durationInSec) {
		this.durationInSec = durationInSec;
	}
	public String getsDUploadStatus() {
		return sDUploadStatus;
	}
	public void setsDUploadStatus(String sDUploadStatus) {
		this.sDUploadStatus = sDUploadStatus;
	}
	public String getDownloadedDate() {
		return downloadedDate;
	}
	public void setDownloadedDate(String downloadedDate) {
		this.downloadedDate = downloadedDate;
	}
	public String getSharedDriveFileLocation() {
		return sharedDriveFileLocation;
	}
	public void setSharedDriveFileLocation(String sharedDriveFileLocation) {
		this.sharedDriveFileLocation = sharedDriveFileLocation;
	}
	public String getHorreumUploadStatus() {
		return horreumUploadStatus;
	}
	public void setHorreumUploadStatus(String horreumUploadStatus) {
		this.horreumUploadStatus = horreumUploadStatus;
	}
	public String getDownloadedFileName() {
		return downloadedFileName;
	}
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}
}
